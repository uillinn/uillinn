use std::fmt;

/// Represents either a single value repeated 4 times or 4 different values (used to specify
/// borders)
#[derive(Debug, Clone, Hash, PartialEq)]
pub enum AllOrEach<T> {
    /// Set all values at once
    All(T),

    /// Set each value independently
    Each(T, T, T, T),
}

impl<T> AllOrEach<T> {
    /// Convert an `AllOrEach` to a tuple, filling it with the same value if this is a
    /// `AllOrEach::All`
    pub fn to_tuple(&self) -> (&T, &T, &T, &T) {
        match self {
            Self::All(val) => (val, val, val, val),
            Self::Each(v1, v2, v3, v4) => (v1, v2, v3, v4),
        }
    }
}

impl<T> From<T> for AllOrEach<T> {
    fn from(val: T) -> Self {
        Self::All(val)
    }
}

/// Horizontal text alignment relative to its area
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub enum Alignment {
    /// The text will be positioned at the left of the area (default for [`Paragraph`])
    ///
    /// <div style="width: 100px; height: auto; background-color: #ffab4c;">
    ///   <p style="text-align: left;">Hello!</p>
    /// </div>
    ///
    /// [`Paragraph`]: super::Paragraph
    Left,

    /// The text will be positioned at the center of the area (default for [`Text`])
    ///
    /// <div style="width: 100px; height: auto; background-color: #ffab4c;">
    ///   <p style="text-align: center;">Hello!</p>
    /// </div>
    ///
    /// [`Text`]: super::Text
    Center,

    /// The text will be positioned at the right of the area
    ///
    /// <div style="width: 100px; height: auto; background-color: #ffab4c;">
    ///   <p style="text-align: right;">Hello!</p>
    /// </div>
    Right,
}

impl fmt::Display for Alignment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Left => "left",
                Self::Center => "center",
                Self::Right => "right",
            }
        )
    }
}

/// A list of font families
///
/// It can be built using one of the four `From` implementations:
/// - From a `Vec<String>`, you can specify multiple fallback fonts, the first matched will be used
/// - From a `Vec<&'static str>`, you can specify multiple fallback fonts, the first matched will be used
/// - From a `String`, only one font family should be specified
/// - From a `&'static str`, only one font family should be specified
#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct FontFamilyList(Vec<String>);

impl FontFamilyList {
    /// Iterate the font family list
    pub fn to_iter(&self) -> impl Iterator<Item = &String> {
        self.0.iter()
    }
}

impl From<Vec<String>> for FontFamilyList {
    fn from(val: Vec<String>) -> Self {
        Self(val)
    }
}

impl From<Vec<&'static str>> for FontFamilyList {
    fn from(val: Vec<&'static str>) -> Self {
        Self(val.iter().map(|f| f.to_string()).collect::<Vec<String>>())
    }
}

impl From<String> for FontFamilyList {
    fn from(val: String) -> Self {
        Self(vec![val])
    }
}

impl From<&'static str> for FontFamilyList {
    fn from(val: &'static str) -> Self {
        Self(vec![val.to_string()])
    }
}

impl fmt::Display for FontFamilyList {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0.join(","))
    }
}
