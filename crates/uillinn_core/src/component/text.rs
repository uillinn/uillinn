//! Definition of the `Text` component
//!
//! Defaults:
//! - Color: `black`
//! - Font family: `sans-serif`
//! - Font size: `16`
//! - Font weight: `400`
//! - Alignment: `center`
//! - Rotation: `0`
use super::{Alignment, Component, ComponentKind, CustomComponent, FontFamilyList};
use crate::color::Color;

/// A single line of text
///
/// If its content is overflowing the area, the overflowing part will be hidden.
/// The text will always remains on the same line.
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Text {
    /// The content displayed
    pub content: String,

    /// The color of the characters
    pub color: Color,

    /// The font family (and the fallback fonts families) used to render characters
    pub font_family: FontFamilyList,

    /// The height of the text characters (in pixels)
    pub font_size: usize,

    /// The thickness of the characters (in pixels)
    ///
    /// Should be between 100 and 900
    ///
    /// Should be a multiple of 100
    pub font_weight: usize,

    /// The alignment of the content
    pub align: Alignment,

    /// The rotation of the text
    pub rotation: isize,
}

impl Text {
    /// Create a new `Text` with the content `content`
    pub fn new<T: Into<String>>(content: T) -> Self {
        Self {
            content: content.into(),
            color: Color::Black,
            font_family: "sans-serif".into(),
            font_size: 16,
            font_weight: 400,
            align: Alignment::Center,
            rotation: 0,
        }
    }

    /// Change the color of the characters
    pub fn color<T: Into<Color>>(mut self, color: T) -> Self {
        self.color = color.into();
        self
    }

    /// Change the font family (and the fallback fonts families) used to render the characters
    pub fn font_family<T: Into<FontFamilyList>>(mut self, font_family: T) -> Self {
        self.font_family = font_family.into();
        self
    }

    /// Change the height of the characters
    pub fn font_size(mut self, font_size: usize) -> Self {
        self.font_size = font_size;
        self
    }

    /// Change the thickness of the characters
    pub fn font_weight(mut self, font_weight: usize) -> Self {
        self.font_weight = font_weight;
        self
    }

    /// Change the alignment of the text
    pub fn align(mut self, align: Alignment) -> Self {
        self.align = align;
        self
    }

    /// Change the rotation of the rectangle
    pub fn rotate(mut self, degrees: isize) -> Self {
        self.rotation = degrees;
        self
    }
}

impl CustomComponent for Text {
    fn build(self) -> Component {
        Component {
            name: super::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Text(self),
            children: None,
        }
    }
}
