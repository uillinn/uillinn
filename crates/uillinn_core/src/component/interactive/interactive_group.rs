//! Definition of the `InteractiveGroup` meta-component
use crate::{component::interactive::InteractiveComponent, prelude::*};
use std::fmt;

/// A kind of interactive action
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum InteractiveActionKind {
    /// Get a thing from the state without modifying it
    GET,

    /// Modify the state
    POST,
}

impl fmt::Display for InteractiveActionKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::GET => "GET",
                Self::POST => "POST",
            }
        )
    }
}

/// An interactive action is executed when an interactive component is enabled
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct InteractiveAction {
    /// The kind of interactive action
    pub kind: InteractiveActionKind,

    /// A path describing which action to run
    pub path: String,
}

impl InteractiveAction {
    /// Create a new [`InteractiveAction`]
    pub fn new<T: Into<String>>(kind: InteractiveActionKind, path: T) -> Self {
        Self {
            kind,
            path: path.into(),
        }
    }
}

/// Define if a child of an interactive group is required to have a value
#[derive(Debug, Clone, Hash, PartialEq)]
pub enum IsRequired {
    /// A value is required
    Required,

    /// A value is not required, a default value is specified instead
    NotRequired(String),
}

/// A group of several named interactive components with a component which triggers an action with
/// them
///
/// # Example
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// let text_input = TextInput::new();
/// let group = InteractiveGroup::new(POST, "/new")
///     .child(
///         "my-input",
///         Required,
///         Area::new(0, 0, 300, 50),
///         text_input,
///     )
///     .child_trigger(Area::new(CENTER, 75, 50, 20), Rectangle::new().color(Blue));
/// ```
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct InteractiveGroup {
    /// The children belonging to the group
    pub children: Vec<(String, IsRequired, (Area, Component))>,

    /// A component executing the action of the group when pressed
    pub trigger: Option<Box<(Area, Component)>>,

    /// An action done when the trigger component is pressed
    pub action: InteractiveAction,
}

impl InteractiveGroup {
    /// Create a new default `InteractiveGroup`
    pub fn new<T: Into<String>>(kind: InteractiveActionKind, path: T) -> Self {
        Self {
            children: vec![],
            trigger: None,
            action: InteractiveAction::new(kind, path),
        }
    }

    /// Add an interactive component in the group
    pub fn child<T1: Into<String>, T2: InteractiveComponent>(
        mut self,
        name: T1,
        is_required: IsRequired,
        area: Area,
        component: T2,
    ) -> Self {
        self.children
            .push((name.into(), is_required, (area, component.build())));
        self
    }

    /// Add a trigger component to the group to execute its action
    pub fn child_trigger<C: CustomComponent>(mut self, area: Area, component: C) -> Self {
        self.trigger = Some(Box::new((area, component.build())));
        self
    }
}

impl CustomComponent for InteractiveGroup {
    fn build(self) -> Component {
        if self.trigger.is_none() {
            // TODO: Do we need to propagate the error?
            // TODO: Display the name of the wrong group
            panic!("An `InteractiveGroup` must have a component triggering its action");
        }

        Component {
            name: crate::component::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Interactive(InteractiveComponentKind::InteractiveGroup(self)),
            children: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "An `InteractiveGroup` must have a component triggering its action")]
    fn panic_when_no_trigger() {
        InteractiveGroup::new(GET, "/nothing").build();
    }
}
