//! Define all components which the user can interact with
//!
//! Note that all interactive components have no appearance, they are just placed on top (or behind) another displayed component
pub mod boolean_input;
pub mod interactive_group;
pub mod text_input;

pub use boolean_input::BooleanInput;
pub use interactive_group::{
    InteractiveAction, InteractiveActionKind, InteractiveGroup, IsRequired,
};
pub use text_input::TextInput;

/// The type of an interactive component
#[derive(Debug, Clone, Hash, PartialEq)]
pub enum InteractiveComponentKind {
    /// [Learn more][`TextInput`]
    TextInput(TextInput),

    /// [Learn more][`BooleanInput`]
    BooleanInput(BooleanInput),

    /// [Learn more][`InteractiveGroup`]
    InteractiveGroup(InteractiveGroup),
}

/// Defines a custom interactive component
pub trait InteractiveComponent: super::CustomComponent {}
