//! Define all base components, how they are represented and some data structures used by them
//!
//! There are 5 components:
//!
//! - [`Text`]
//! - [`Rectangle`]
//! - [`Ellipse`]
//! - [`Path`]
//! - [`Paragraph`]
//!
//! If you want to transform an arbitrary structure into a component, you can implement the
//! [`CustomComponent`] trait
pub mod ellipse;
pub mod interactive;
pub mod paragraph;
pub mod path;
pub mod rectangle;
pub mod text;
mod utils;

use super::layout::Area;
pub use utils::*;

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub use ellipse::Ellipse;
pub use interactive::*;
pub use paragraph::Paragraph;
pub use path::Path;
pub use rectangle::Rectangle;
pub use text::Text;

const HASH_START: &str = "ui-";
const DEFAULT_NAME: &str = "unnamed";

/// Define a common interface to transform any structure into a `Component` (this transformation
/// happens automatically in the [`Component::child`] method)
///
/// # Example for defining a custom component with a structure
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// struct Button {
///     label: String,
/// }
///
/// impl Button {
///     fn new<T: Into<String>>(label: T) -> Self {
///         Self {
///             label: label.into(),
///         }
///     }
/// }
///
/// impl CustomComponent for Button {
///     fn build(self) -> Component {
///         Component::new()
///             .child(Area::new(0, 0, 100, 50), Rectangle::new().color(Blue).rounded(20))
///             .child(Area::new(0, 0, 100, 50), Text::new(self.label).color(White))
///     }
/// }
///
/// let my_button = Button::new("Learn more");
/// let my_page = Component::new().child(Area::new(0, 0, 100, 50), my_button);
/// ```
pub trait CustomComponent {
    /// Turn any structure into a [`Component`]
    fn build(self) -> Component;
}

/// The type of a component and its styles (how to render it)
#[derive(Debug, Clone, Hash, PartialEq)]
pub enum ComponentKind {
    /// [Learn more][`Text`]
    Text(Text),

    /// [Learn more][`Paragraph`]
    Paragraph(Paragraph),

    /// [Learn more][`Rectangle`]
    Rectangle(Rectangle),

    /// [Learn more][`Ellipse`]
    Ellipse(Ellipse),

    /// [Learn more][`Ellipse`]
    Path(Path),

    /// An interactive component
    ///
    /// [Learn more][`crate::component::interactive::InteractiveComponentKind`]
    Interactive(InteractiveComponentKind),

    /// A container for other components
    Custom,
}

/// A component is a small portion of a user-interface
///
/// It is designed to be independent of other components and to be recursively rendered
///
/// [`Component::new`] is used to create a container (just an element containing other elements but
/// without styles) and the `new` method of the re-exported structures in [`uillinn_core::component`]
/// are used to add base elements
///
/// [`uillinn_core::component`]: crate::component
///
/// # Example
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// let _ = Component::new()
///     .child(Area::new(0, 0, 100, 50), Rectangle::new().color(Rgb(0, 255, 255)))
///     .child(Area::new(10, 10, 80, 30), Text::new("Click me!"));
/// ```
///
/// Because the application is written in Rust, you can return a `Component` from a function and
/// use it in other functions to create reusable components
///
/// # Example
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// fn button(text: &str) -> Component {
///     Component::new()
///         .child(Area::new(0, 0, 100, 50), Rectangle::new().color(Red))
///         .child(Area::new(10, 10, 80, 30), Text::new(text))
/// }
///
/// Component::new()
///     .child(
///         Area::new(0, 0, Percent(40), Percent(50)),
///         button("Hello world!"),
///     )
///     .child(
///         Area::new(StartPercent(60), 0, Percent(40), Percent(50)),
///         button("Click me!"),
///     );
/// ```

#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Component {
    /// The name of the component
    pub name: String,

    /// The type of component
    pub kind: ComponentKind,

    /// The children belonging to this component
    pub children: Option<Vec<(Area, Component)>>,
}

impl Component {
    /// Create a new component used like a container
    pub fn new() -> Self {
        Self::default()
    }

    /// Set the name of the component
    pub fn set_name<T: Into<String>>(mut self, name: T) -> Self {
        self.name = name.into();
        self
    }

    /// Add a child to this component
    ///
    /// If two or more children are placed on the same location on the screen, the last of the chain of
    /// `.child` will be on top of the others.
    ///
    /// # Panics
    ///
    /// Panics if you try to add a child to a base component
    ///
    /// ```rust,should_panic
    /// use uillinn_core::prelude::*;
    ///
    /// let _ = Text::new("Some text")
    ///     .build()
    ///     .child(Area::new(0, 0, 100, 50), Rectangle::new().color(Red));
    /// ```
    pub fn child<C: CustomComponent>(mut self, area: Area, component: C) -> Self {
        if let Some(children) = self.children.as_mut() {
            children.push((area, component.build()));
            self
        } else {
            panic!("You cannot add children to {:?}", self.kind);
        }
    }

    /// Get a hash (a unique identifier) of this component
    pub fn get_hash(&self, area: &Area) -> String {
        let mut s = DefaultHasher::new();
        (self, area).hash(&mut s);
        let hash = s.finish();

        // Prevent the hash from beginning with a number
        format!("{}{:x}", HASH_START, hash)
    }
}

impl CustomComponent for Component {
    fn build(self) -> Component {
        self
    }
}

impl Default for Component {
    fn default() -> Self {
        Self {
            name: DEFAULT_NAME.to_string(),
            kind: ComponentKind::Custom,
            children: Some(vec![]),
        }
    }
}
