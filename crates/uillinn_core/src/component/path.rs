//! Definition of the `Path` component
//!
//! Defaults:
//! - Color: `black`
//! - Stroke color: `black`
//! - Stroke size: `0`
//! - Rotation: `0`
use super::{Component, ComponentKind, CustomComponent};
use crate::color::Color;

/// A drawing defined using points, lines and curves
///
/// A path does not need to be closed for being filled
///
/// This component is based on [SVG paths](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths)
///
/// NOTE: Each method ending with `_by` is relative to the previous point and each method ending with `_to` is absolute.
/// Negative values goes to left/top and positive ones goes to right/bottom
///
/// # Example
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// let path = Path::new((20, 20))
///     .move_by(60., 100.)
///     .y_line_by(-60.)
///     .cubic_by((0., -20.), (10., -20.), (10., 0.))
///     .y_line_by(60.)
///     .x_line_by(-10.)
///     .move_by(10., 0.)
///     .cubic_by((0., 20.), (10., 20.), (10., 0.))
///     .y_line_by(-60.)
///     .x_line_by(-10.)
///     .move_by(10., 0.)
///     .fill_color(Navy);
///
/// assert_eq!(path.to_svg_path(), "m0,0 m60,100 v-60 c0,-20 10,-20 10,0 v60 h-10 m10,0 c0,20 10,20 10,0 v-60 h-10 m10,0");
/// ```
///
/// <svg width="150" height="150" style="border: 2px solid #1a1a1a;">
///     <path d="m0,0 m60,100 v-60 c0,-20 10,-20 10,0 v60 h-10 m10,0 c0,20 10,20 10,0 v-60 h-10 m10,0" style="fill:#001f3f;" />
/// </svg>
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Path {
    /// The background color between the strokes
    ///
    /// The path does not need to be closed for being filled
    pub fill_color: Color,

    /// The stroke color
    pub stroke_color: Color,

    /// The stroke size (in pixels)
    pub stroke_size: usize,

    /// The bounding box of the inner path (like `viewBox` in SVG)
    ///
    /// Note: It must includes the size of the stroke
    pub bounding_box: (usize, usize),

    /// The shape of the path (using SVG path data)
    data: Vec<String>,

    /// Prevent closing the path
    prevent_close: bool,
}

impl Path {
    /// Create a new `Path` with a bounding box
    pub fn new(bounding_box: (usize, usize)) -> Self {
        Self {
            fill_color: Color::Black,
            stroke_color: Color::Black,
            stroke_size: 0,
            bounding_box,
            data: vec!["m0,0".to_string()], // Begin the path at the start of the component
            prevent_close: false,
        }
    }

    /// Create a new `Path` from an SVG path data
    ///
    /// # Example
    ///
    /// The SVG example path data comes from [https://yqnn.github.io/svg-path-editor](https://yqnn.github.io/svg-path-editor) (it was scaled 5 times).
    ///
    /// ```rust
    /// use uillinn_core::prelude::*;
    ///
    /// let svg_path = "M 20 40 L 50 5 L 65 0 L 60 15 L 25 45 C 30 50 30 55 35 50 C 35 55 40 60 35 60 A 7.1 7.1 90 0 1 30 65 A 25 25 90 0 0 20 50 Q 17.5 49.5 17.5 52.5 T 10 59 T 6 55 T 12.5 47.5 T 15 45 A 25 25 90 0 0 0 35 A 7.1 7.1 90 0 1 5 30 C 5 25 10 30 15 30 C 10 35 15 35 20 40 M 50 5 L 50 15 L 60 15 L 51 14 L 50 5";
    /// let my_path = Path::from_svg_path((70, 70), svg_path).fill_color(Transparent).stroke_size(2);
    /// assert_eq!(my_path.to_svg_path(), svg_path);
    /// ```
    ///
    /// <svg width="70" height="70"><path d="M 20 40 L 50 5 L 65 0 L 60 15 L 25 45 C 30 50 30 55 35 50 C 35 55 40 60 35 60 A 7.1 7.1 90 0 1 30 65 A 25 25 90 0 0 20 50 Q 17.5 49.5 17.5 52.5 T 10 59 T 6 55 T 12.5 47.5 T 15 45 A 25 25 90 0 0 0 35 A 7.1 7.1 90 0 1 5 30 C 5 25 10 30 15 30 C 10 35 15 35 20 40 M 50 5 L 50 15 L 60 15 L 51 14 L 50 5" fill="transparent" stroke="rgb(0,0,0)" stroke-width="2" /></svg>
    pub fn from_svg_path<T: Into<String>>(bounding_box: (usize, usize), data: T) -> Self {
        let data = data.into();
        Self {
            fill_color: Color::Black,
            stroke_color: Color::Black,
            stroke_size: 0,
            bounding_box,
            data: if data.starts_with('m') || data.starts_with('M') {
                vec![data]
            } else {
                vec!["m0,0".to_string(), data]
            },
            prevent_close: false,
        }
    }

    /// Change the background color between the strokes
    pub fn fill_color<T: Into<Color>>(mut self, fill_color: T) -> Self {
        self.fill_color = fill_color.into();
        self
    }

    /// Change the stroke color
    pub fn stroke_color<T: Into<Color>>(mut self, stroke_color: T) -> Self {
        self.stroke_color = stroke_color.into();
        self
    }

    /// Change the stroke size
    pub fn stroke_size(mut self, stroke_size: usize) -> Self {
        self.stroke_size = stroke_size;
        self
    }

    /// Returns an owned `String` containing the SVG compatible path
    pub fn to_svg_path(&self) -> String {
        self.data.join(" ")
    }

    // Path manipulation

    /// Move from the current position to (x, y)
    pub fn move_by(mut self, x: f32, y: f32) -> Self {
        self.data.push(format!("m{},{}", x, y));
        self
    }

    /// Move to (x, y)
    pub fn move_to(mut self, x: f32, y: f32) -> Self {
        self.data.push(format!("M{},{}", x, y));
        self
    }

    /// Draw a line from the current position to (x, y)
    pub fn line_by(mut self, x: f32, y: f32) -> Self {
        self.data.push(format!("l{},{}", x, y));
        self
    }

    /// Draw a line to (x, y)
    pub fn line_to(mut self, x: f32, y: f32) -> Self {
        self.data.push(format!("L{},{}", x, y));
        self
    }

    /// Draw an horizontal line of width `w` from the current position
    pub fn x_line_by(mut self, w: f32) -> Self {
        self.data.push(format!("h{}", w));
        self
    }

    /// Draw an horizontal line of width `w`
    pub fn x_line_to(mut self, w: f32) -> Self {
        self.data.push(format!("H{}", w));
        self
    }

    /// Draw an vertical line of height `h` from the current position
    pub fn y_line_by(mut self, h: f32) -> Self {
        self.data.push(format!("v{}", h));
        self
    }

    /// Draw an vertical line of height `h`
    pub fn y_line_to(mut self, h: f32) -> Self {
        self.data.push(format!("V{}", h));
        self
    }

    /// Draw a cubic bézier curve from the current position to (end_x, end_y) with the first
    /// control point at (control_point_1_x, control_point_1_y) and the second at
    /// (control_point_2_x, control_point_2_y)
    ///
    /// <img src="https://svgpocketguide.com/images/curvecubic.png" />
    ///
    /// (From [https://svgpocketguide.com](https://svgpocketguide.com), under CC-BY-NC-SA 4.0)
    pub fn cubic_by(
        mut self,
        control_point_1: (f32, f32),
        control_point_2: (f32, f32),
        end: (f32, f32),
    ) -> Self {
        self.data.push(format!(
            "c{},{} {},{} {},{}",
            control_point_1.0,
            control_point_1.1,
            control_point_2.0,
            control_point_2.1,
            end.0,
            end.1
        ));
        self
    }

    /// Draw a cubic bézier curve to (end_x, end_y) with the first
    /// control point at (control_point_1_x, control_point_1_y) and the second at
    /// (control_point_2_x, control_point_2_y)
    ///
    /// <img src="https://svgpocketguide.com/images/curvecubic.png" />
    ///
    /// (From [https://svgpocketguide.com](https://svgpocketguide.com), under CC-BY-NC-SA 4.0)
    pub fn cubic_to(
        mut self,
        control_point_1: (f32, f32),
        control_point_2: (f32, f32),
        end: (f32, f32),
    ) -> Self {
        self.data.push(format!(
            "C{},{} {},{} {},{}",
            control_point_1.0,
            control_point_1.1,
            control_point_2.0,
            control_point_2.1,
            end.0,
            end.1
        ));
        self
    }

    /// Draw a quadratic bézier curve from the current position to (end_x, end_y) with the
    /// control point at (control_point_x, control_point_y)
    ///
    /// <img src="https://svgpocketguide.com/images/curvequad.png" />
    ///
    /// (From [https://svgpocketguide.com](https://svgpocketguide.com), under CC-BY-NC-SA 4.0)
    pub fn quadratic_by(mut self, control_point: (f32, f32), end: (f32, f32)) -> Self {
        self.data.push(format!(
            "q{},{} {},{}",
            control_point.0, control_point.1, end.0, end.1
        ));
        self
    }

    /// Draw a quadratic bézier curve to (end_x, end_y) with the
    /// control point at (control_point_x, control_point_y)
    ///
    /// <img src="https://svgpocketguide.com/images/curvequad.png" />
    ///
    /// (From [https://svgpocketguide.com](https://svgpocketguide.com), under CC-BY-NC-SA 4.0)
    pub fn quadratic_to(mut self, control_point: (f32, f32), end: (f32, f32)) -> Self {
        self.data.push(format!(
            "Q{},{} {},{}",
            control_point.0, control_point.1, end.0, end.1
        ));
        self
    }

    /// Close the path
    pub fn close(mut self) -> Self {
        self.data.push("z".to_string());
        self
    }
}

impl CustomComponent for Path {
    fn build(self) -> Component {
        // TODO: Compute the bounding box automagically
        let mut path = self;

        // If the path specified starts with a move command, we can remove the useless `m0,0`
        // added before
        if path.data.len() >= 2 && (path.data[1].starts_with('m') || path.data[1].starts_with('M'))
        {
            path.data.remove(0);
        }

        Component {
            name: super::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Path(path),
            children: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    fn path_helper(path: Path) -> String {
        if let ComponentKind::Path(path) = path.build().kind {
            path.to_svg_path()
        } else {
            unreachable!();
        }
    }

    #[test]
    fn move_cmd() {
        let path = Path::new((20, 20)).move_by(20., 20.).close();
        assert_eq!(path_helper(path), "m20,20 z".to_string());

        let path = Path::new((20, 20)).move_to(20., 20.);
        assert_eq!(path_helper(path), "M20,20".to_string());
    }

    #[test]
    fn line_cmd() {
        let path = Path::new((20, 20)).line_by(20., 20.).close();
        assert_eq!(path_helper(path), "m0,0 l20,20 z".to_string());

        let path = Path::new((20, 20)).line_to(20., 20.);
        assert_eq!(path_helper(path), "m0,0 L20,20".to_string());
    }

    #[test]
    fn x_line_cmd() {
        let path = Path::new((20, 20)).x_line_by(20.).close();
        assert_eq!(path_helper(path), "m0,0 h20 z".to_string());

        let path = Path::new((20, 20)).x_line_to(20.);
        assert_eq!(path_helper(path), "m0,0 H20".to_string());
    }

    #[test]
    fn y_line_cmd() {
        let path = Path::new((20, 20)).y_line_by(20.).close();
        assert_eq!(path_helper(path), "m0,0 v20 z".to_string());

        let path = Path::new((20, 20)).y_line_to(20.);
        assert_eq!(path_helper(path), "m0,0 V20".to_string());
    }

    #[test]
    fn cubic_bezier_cmd() {
        let path = Path::new((20, 20))
            .cubic_by((10., 0.), (10., 10.), (0., 10.))
            .close();
        assert_eq!(path_helper(path), "m0,0 c10,0 10,10 0,10 z".to_string());

        let path = Path::new((20, 20)).cubic_to((10., 0.), (10., 10.), (0., 10.));
        assert_eq!(path_helper(path), "m0,0 C10,0 10,10 0,10".to_string());
    }

    #[test]
    fn quadratic_bezier_cmd() {
        let path = Path::new((20, 20))
            .quadratic_by((10., 5.), (0., 10.))
            .close();
        assert_eq!(path_helper(path), "m0,0 q10,5 0,10 z".to_string());

        let path = Path::new((20, 20)).quadratic_to((10., 5.), (0., 10.));
        assert_eq!(path_helper(path), "m0,0 Q10,5 0,10".to_string());
    }

    #[test]
    fn from_svg_path_test() {
        let path = Path::from_svg_path((23, 23), "m12,2 l10,15 h-20 l10,-15 z");
        assert_eq!(
            path_helper(path),
            path_helper(
                Path::new((23, 23))
                    .move_by(12., 2.)
                    .line_by(10., 15.)
                    .x_line_by(-20.)
                    .line_by(10., -15.)
                    .close()
            )
        );

        let path = Path::from_svg_path((23, 23), "l 10 15 h -20 l 10 -15 z");
        assert_eq!(path.data[0], "m0,0");
    }
}
