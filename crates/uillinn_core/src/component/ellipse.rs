//! Definition of the `Ellipse` component
//!
//! Defaults:
//! - Color: `black`
//! - Border size: `0`
//! - Border color `black`
//! - Rotation: `0`
//! - Start angle: `0`
//! - End angle: `360`
//! - Ratio: `0`
use super::{Component, ComponentKind, CustomComponent};
use crate::color::Color;

/// An ellipse filled or stroked
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Ellipse {
    /// The inner color of the ellipse
    pub color: Color,

    /// The size of the border (in pixels)
    pub border_size: usize,

    /// The color of the border
    pub border_color: Color,

    /// The rotation of the ellipse (used for borders)
    pub rotation: isize,

    /// Draw only a part of the ellipse starting from this angle
    ///
    /// Must not exceed 360
    pub start_angle: usize,

    /// Draw only a part of the ellipse ending at this angle
    ///
    /// Must not exceed 360
    pub end_angle: usize,

    /// The ratio between the filled part of the ellipse and its borders
    ///
    /// Must not exceed 100
    pub ratio: usize,
}

impl Ellipse {
    /// Create a new default `Ellipse`
    pub fn new() -> Self {
        Self::default()
    }

    /// Change the background color of the ellipse
    pub fn color<T: Into<Color>>(mut self, color: T) -> Self {
        self.color = color.into();
        self
    }

    /// Change the size of the borders
    pub fn border_size(mut self, border_size: usize) -> Self {
        self.border_size = border_size;
        self
    }

    /// Change the color of the borders
    pub fn border_color(mut self, border_color: Color) -> Self {
        self.border_color = border_color;
        self
    }

    /// Change the rotation of the ellipse
    pub fn rotate(mut self, degrees: isize) -> Self {
        self.rotation = degrees;
        self
    }

    /// Change the start angle
    pub fn start_angle(mut self, degrees: usize) -> Self {
        if degrees > 360 {
            // TODO: Do we need to propagate the error?
            panic!("The start angle of an ellipse must not exceed 360");
        }

        if degrees > self.end_angle {
            // TODO: Do we need to propagate the error?
            panic!("The start angle of an ellipse must not be more than the end angle");
        }

        self.start_angle = degrees;
        self
    }

    /// Change the end angle
    pub fn end_angle(mut self, degrees: usize) -> Self {
        if degrees > 360 {
            // TODO: Do we need to propagate the error?
            panic!("The end angle of an ellipse must not exceed 360");
        }

        if degrees < self.start_angle {
            // TODO: Do we need to propagate the error?
            panic!("The end angle of an ellipse must not be less than the start angle");
        }

        self.end_angle = degrees;
        self
    }

    /// Change the ratio
    pub fn ratio(mut self, percent: usize) -> Self {
        if percent > 100 {
            // TODO: Do we need to propagate the error?
            panic!("The ratio of an ellipse must not exceed 100");
        }

        self.ratio = percent;
        self
    }
}

impl CustomComponent for Ellipse {
    fn build(self) -> Component {
        Component {
            name: super::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Ellipse(self),
            children: None,
        }
    }
}

impl Default for Ellipse {
    fn default() -> Self {
        Self {
            color: Color::Black,
            border_size: 0,
            border_color: Color::Black,
            rotation: 0,
            start_angle: 0,
            end_angle: 360,
            ratio: 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "The start angle of an ellipse must not exceed 360")]
    fn panic_when_start_angle_more_than_360() {
        Ellipse::new().start_angle(500);
    }

    #[test]
    #[should_panic(expected = "The end angle of an ellipse must not exceed 360")]
    fn panic_when_end_angle_more_than_360() {
        Ellipse::new().end_angle(500);
    }

    #[test]
    #[should_panic(expected = "The start angle of an ellipse must not be more than the end angle")]
    fn panic_when_start_angle_more_than_end_angle() {
        Ellipse::new().end_angle(10).start_angle(20);
    }

    #[test]
    #[should_panic(expected = "The end angle of an ellipse must not be less than the start angle")]
    fn panic_when_end_angle_less_than_start_angle() {
        Ellipse::new().start_angle(20).end_angle(10);
    }

    #[test]
    #[should_panic(expected = "The ratio of an ellipse must not exceed 100")]
    fn panic_when_ratio_more_than_100() {
        Ellipse::new().ratio(200);
    }
}
