//! Definition of the `Paragraph` component
//!
//! Defaults:
//! - Color: `black`
//! - Font family: `serif`
//! - Font size: `16`
//! - Font weight: `400`
//! - Alignment: `left`
//! - Break word: `false`
//! - Line height: `120`
//! - Rotation: `0`
use super::{Alignment, Component, ComponentKind, CustomComponent, FontFamilyList};
use crate::color::Color;

/// A paragraph with several lines of text
///
/// If its content is overflowing the area, `…` will be inserted at the end of it
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Paragraph {
    /// The content displayed
    pub content: String,

    /// The color of the characters
    pub color: Color,

    /// The font family (and the fallback fonts families) used to render characters
    pub font_family: FontFamilyList,

    /// The height of the text characters (in pixels)
    pub font_size: usize,

    /// The thickness of the text characters (in pixels)
    ///
    /// Should be between 100 and 900
    ///
    /// Should be a multiple of 100
    pub font_weight: usize,

    /// The alignment of the content
    pub align: Alignment,

    /// Should an overflow break the last word or not?
    pub break_word: bool,

    /// The minimum height of a single line of text
    ///
    /// The value is a percentage of the font size (`150` means 1.5 * font_size)
    pub line_height: usize,

    /// The rotation of the paragraph
    pub rotation: isize,
}

impl Paragraph {
    /// Create a new default `Paragraph` with the content `content`
    pub fn new<T: Into<String>>(content: T) -> Self {
        Self {
            content: content.into(),
            color: Color::Black,
            font_family: "serif".into(),
            font_size: 16,
            font_weight: 400,
            align: Alignment::Left,
            break_word: false,
            line_height: 120,
            rotation: 0,
        }
    }

    /// Change the color of the characters
    pub fn color<T: Into<Color>>(mut self, color: T) -> Self {
        self.color = color.into();
        self
    }

    /// Change the font family (and the fallback fonts families) used to render the characters
    pub fn font_family<T: Into<FontFamilyList>>(mut self, font_family: T) -> Self {
        self.font_family = font_family.into();
        self
    }

    /// Change the height of the characters
    pub fn font_size(mut self, font_size: usize) -> Self {
        self.font_size = font_size;
        self
    }

    /// Change the thickness of the characters
    pub fn font_weight(mut self, font_weight: usize) -> Self {
        self.font_weight = font_weight;
        self
    }

    /// Change the alignment of the text
    pub fn align(mut self, align: Alignment) -> Self {
        self.align = align;
        self
    }

    /// If the text overflows the line, break the word and finish it on a new line
    pub fn break_word(mut self) -> Self {
        self.break_word = true;
        self
    }

    /// Change the line height of the paragraph
    ///
    /// The value is a percentage of the font size (`150` means 1.5 * font_size)
    pub fn line_height(mut self, val: usize) -> Self {
        self.line_height = val;
        self
    }

    /// Change the rotation of the paragraph
    pub fn rotate(mut self, degrees: isize) -> Self {
        self.rotation = degrees;
        self
    }
}

impl CustomComponent for Paragraph {
    fn build(self) -> Component {
        Component {
            name: super::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Paragraph(self),
            children: None,
        }
    }
}
