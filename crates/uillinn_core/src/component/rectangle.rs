//! Definition of the `Rectangle` component
//!
//! Defaults:
//! - Color: `black`
//! - Border size: `0`
//! - Border color `black`
//! - Radius of the angles: `0` for all
//! - Rotation: `0`
use super::{AllOrEach, Component, ComponentKind, CustomComponent};
use crate::color::Color;

/// A rectangle filled or stroked
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Rectangle {
    /// The inner color of the rectangle
    pub color: Color,

    /// The size of the borders (in pixels)
    pub border_size: usize,

    /// The color of the borders
    pub border_color: Color,

    /// Radius of the borders (in pixels)
    pub rounded: AllOrEach<usize>,

    /// The rotation of the rectangle
    pub rotation: isize,
}

impl Rectangle {
    /// Create a new default `Rectangle`
    pub fn new() -> Self {
        Self::default()
    }

    /// Change the background color of the rectangle
    pub fn color<T: Into<Color>>(mut self, color: T) -> Self {
        self.color = color.into();
        self
    }

    /// Change the size of the borders
    pub fn border_size(mut self, border_size: usize) -> Self {
        self.border_size = border_size;
        self
    }

    /// Change the color of the borders
    pub fn border_color(mut self, border_color: Color) -> Self {
        self.border_color = border_color;
        self
    }

    /// Change the radius of the angles
    pub fn rounded<T: Into<AllOrEach<usize>>>(mut self, rounded: T) -> Self {
        self.rounded = rounded.into();
        self
    }

    /// Change the rotation of the rectangle
    pub fn rotate(mut self, degrees: isize) -> Self {
        self.rotation = degrees;
        self
    }
}

impl CustomComponent for Rectangle {
    fn build(self) -> Component {
        Component {
            name: super::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Rectangle(self),
            children: None,
        }
    }
}

impl Default for Rectangle {
    fn default() -> Self {
        Self {
            color: Color::Black,
            border_size: 0,
            border_color: Color::Black,
            rounded: AllOrEach::All(0),
            rotation: 0,
        }
    }
}
