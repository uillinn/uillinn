//! Definition of an application
use crate::prelude::Component;

/// Structure containing the tree of components and storing some metadata about the application
/// (used for accessibility and SEO)
#[derive(Debug, PartialEq)]
pub struct App {
    /// The name of the application
    pub name: String,

    /// The description of the application
    pub description: String,

    /// The language of the application
    pub lang: String,

    /// The root component of the tree
    pub root: Component,
}

impl App {
    /// Create a new `App` named `name`, with the description `description`, the language `lang`
    /// and with the root component `root`
    pub fn new<T1: Into<String>, T2: Into<String>, T3: Into<String>>(
        name: T1,
        description: T2,
        lang: T3,
        root: Component,
    ) -> Self {
        Self {
            name: name.into(),
            description: description.into(),
            lang: lang.into(),
            root,
        }
    }
}
