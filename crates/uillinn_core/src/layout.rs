//! Define how components are positioned and sized on the screen

/// Alias used to center things easily (it is aliased to `Pos::CenterPx(0)`)
pub const CENTER: Pos = Pos::CenterPx(0);

/// Alias used to let things fill the parent area easily (it is aliased to `Size::Percent(100)`)
pub const FULL: Size = Size::Percent(100);

/// The position of a component along an axis on the screen
#[derive(Debug, Hash, Copy, Clone, PartialEq)]
pub enum Pos {
    /// Position from the left/top border of the parent (in pixels)
    StartPx(isize),

    /// Position from the left/top border of the parent (in percents)
    StartPercent(isize),

    /// Position from the center of the parent (in pixels)
    CenterPx(isize),

    /// Position from the center of the parent (in percents)
    CenterPercent(isize),

    /// Position from the right/bottom border of the parent (in pixels)
    EndPx(isize),

    /// Position from the right/bottom border of the parent (in percents)
    EndPercent(isize),
}

impl From<isize> for Pos {
    fn from(v: isize) -> Self {
        Self::StartPx(v)
    }
}

/// The size of a component along an axis on the screen
#[derive(Debug, Hash, Copy, Clone, PartialEq)]
pub enum Size {
    /// Size in pixels
    Px(usize),

    /// Size in percents
    Percent(usize),
}

impl From<usize> for Size {
    fn from(v: usize) -> Self {
        Self::Px(v)
    }
}

/// A type of screen based breakpoint
#[derive(Debug, Hash, Copy, Clone, PartialEq)]
pub enum ScreenBasedKind<T> {
    /// If the width of the screen is smaller than .0, .1 value is used
    WidthSmallerThan(usize, T),

    /// If the width of the screen is wider than .0, .1 value is used
    WidthWiderThan(usize, T),

    /// If the height of the screen is smaller than .0, .1 value is used
    HeightSmallerThan(usize, T),

    /// If the height of the screen is wider than .0, .1 value is used
    HeightWiderThan(usize, T),
}

/// A position or size based on the size of the screen (used to create responsive UIs)
///
/// For the same breakpoint kind, all values are evaluated from top to bottom:
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // If the width of the screen is smaller than 1000px, the element will have a width of 20px,
/// // if the width of the screen is smaller than 770px, the element will have a width of 30px
/// Area::new(
///     0,
///     0,
///     0,
///     ScreenBased::new(10)
///         .width_smaller_than(1000, 20)
///         .width_smaller_than(770, 30),
/// );
///
/// // If the width of the screen is smaller than 1000px, the element will have a width of 20px,
/// // if the width of the screen is smaller than 770px, the element will have a width of **20px**
/// // (1000px is the last, and 770 < 1000, so 1000px wons)
/// Area::new(
///     0,
///     0,
///     0,
///     ScreenBased::new(10)
///         .width_smaller_than(770, 30)
///         .width_smaller_than(1000, 20),
/// );
/// ```
#[derive(Debug, Hash, Clone, PartialEq)]
pub struct ScreenBased<T> {
    /// The default value used when no breakpoint is matched
    pub default: T,

    /// A list of breakpoints and the position or size used for each
    pub breakpoints: Vec<ScreenBasedKind<T>>,
}

impl<T> ScreenBased<T> {
    /// Create a new `ScreenBased` with default used when no breakpoint is matched
    pub fn new<I: Into<T>>(default: I) -> Self {
        Self {
            default: default.into(),
            breakpoints: vec![],
        }
    }

    /// Set a [`ScreenBasedKind::WidthSmallerThan`] rule
    pub fn width_smaller_than<I: Into<T>>(mut self, breakpoint: usize, val: I) -> Self {
        self.breakpoints
            .push(ScreenBasedKind::WidthSmallerThan(breakpoint, val.into()));
        self
    }

    /// Set a [`ScreenBasedKind::WidthWiderThan`] rule
    pub fn width_wider_than<I: Into<T>>(mut self, breakpoint: usize, val: I) -> Self {
        self.breakpoints
            .push(ScreenBasedKind::WidthWiderThan(breakpoint, val.into()));
        self
    }

    /// Set a [`ScreenBasedKind::HeightSmallerThan`] rule
    pub fn height_smaller_than<I: Into<T>>(mut self, breakpoint: usize, val: I) -> Self {
        self.breakpoints
            .push(ScreenBasedKind::HeightSmallerThan(breakpoint, val.into()));
        self
    }

    /// Set a [`ScreenBasedKind::HeightWiderThan`] rule
    pub fn height_wider_than<I: Into<T>>(mut self, breakpoint: usize, val: I) -> Self {
        self.breakpoints
            .push(ScreenBasedKind::HeightWiderThan(breakpoint, val.into()));
        self
    }
}

impl From<Pos> for ScreenBased<Pos> {
    fn from(val: Pos) -> Self {
        Self::new(val)
    }
}

impl From<isize> for ScreenBased<Pos> {
    fn from(val: isize) -> Self {
        Self::new(val)
    }
}

impl From<Size> for ScreenBased<Size> {
    fn from(val: Size) -> Self {
        Self::new(val)
    }
}

impl From<usize> for ScreenBased<Size> {
    fn from(val: usize) -> Self {
        Self::new(val)
    }
}

/// An area of the screen where a component is placed
///
/// A lot of [`From`] implementations are used to make shorthands
///
/// # Examples
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at 0px from the left of the parent area, 0px from the top of the parent area,
/// // with a width of 100px and a height of 100px
/// let area = Area::new(0, 0, 100, 100);
/// ```
///
/// <div style="position:relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div style="position:absolute; left: 0px; top: 0px; width: 100px; height: 100px; background-color: #1a1a1a; border: 1px solid red;"></div>
/// </div>
/// <br />
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at 20% from the left of the parent area, 0px from the top of the parent area,
/// // occupying 80% of the width of the parent area and with a height of 30px
/// let area = Area::new(StartPercent(10), 0, Percent(90), 30);
/// ```
///
/// <div style="position: relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div style="position:absolute; left: 20%; top: 0px; width: 80%; height: 30px; background-color: #1a1a1a; border: 1px solid red;"></div>
/// </div>
/// <br />
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at the center of the parent area, occupying 50% of the width of the
/// // parent area and with a height of 50px (`CENTER` is aliased to `Pos::CenterPx(0)`)
/// let area = Area::new(CENTER, CENTER, Percent(50), 50);
/// ```
///
/// <div style="position: relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div style="position:absolute; left: 50%; top: 50%; width: 50%; height: 50px; transform: translateX(-50%) translateY(-50%); background-color: #1a1a1a; border: 1px solid red;"></div>
/// </div>
/// <br />
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at the center + 35px of the parent area along the X axis, at the center + 10%
/// // of the parent area along the Y axis occupying 50% of the width of the parent area and with a
/// // height of 50px
/// let area = Area::new(CenterPx(35), CenterPercent(10), Percent(50), 50);
/// ```
///
/// <div style="position: relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div style="position:absolute; left: calc(50% + 35px); top: 60%; width: 50%; height: 50px; transform: translateX(-50%) translateY(-50%); background-color: #1a1a1a; border: 1px solid red;"></div>
/// </div>
/// <br />
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at 5% from the right of the parent area, 0px from the bottom of the
/// // parent area, occupying 40% of the width of the parent area and with a height of 60px
/// let area = Area::new(EndPercent(5), EndPx(0), Percent(40), 60);
/// ```
///
/// <div style="position: relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div style="position:absolute; right: 5%; bottom: 0px; width: 40%; height: 60px; background-color: #1a1a1a; border: 1px solid red;"></div>
/// </div>
/// <br />
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// // An area placed at 5% from the right of the parent area, 0px from the bottom of the
/// // parent area, occupying 50px if the screen width is smaller than 400px or 40% of the
/// // parent box otherwise, and with a height of 60px
/// // (try resizing the window to see the change)
/// let area = Area::new(
///     EndPercent(5),
///     EndPx(0),
///     ScreenBased::new(Percent(40)).width_smaller_than(400, 10),
///     60,
/// );
/// ```
///
/// <div style="position: relative; width: 100%; height: 200px; background-color: #ffab4c; border: 1px solid red;">
///   <div id="example-element-screen-based" style="position:absolute; right: 5%; bottom: 0px; width: 40%; height: 60px; background-color: #1a1a1a; border: 1px solid red;"></div>
///   <style>@media screen and (max-width: 400px) { #example-element-screen-based { width: 50px !important; } }</style>
/// </div>
/// <br />
#[derive(Debug, Hash, Clone, PartialEq)]
pub struct Area {
    /// The position of a component along the X axis
    pub x_axis: ScreenBased<Pos>,

    /// The position of a component along the Y axis
    pub y_axis: ScreenBased<Pos>,

    /// The width of the component
    pub width: ScreenBased<Size>,

    /// The height of the component
    pub height: ScreenBased<Size>,

    /// Make the component scrollable (a scrollbar is placed to show the hidden part of the
    /// comonent)
    pub scrollable: bool,
}

impl Area {
    /// Create a new `Area`
    pub fn new<
        T1: Into<ScreenBased<Pos>>,
        T2: Into<ScreenBased<Pos>>,
        T3: Into<ScreenBased<Size>>,
        T4: Into<ScreenBased<Size>>,
    >(
        x_axis: T1,
        y_axis: T2,
        width: T3,
        height: T4,
    ) -> Self {
        Self {
            x_axis: x_axis.into(),
            y_axis: y_axis.into(),
            width: width.into(),
            height: height.into(),
            scrollable: false,
        }
    }

    /// Make the area scrollable
    pub fn scroll(mut self) -> Self {
        self.scrollable = true;
        self
    }
}
