//! Conversions between color spaces

/// Convert RGB colors to HSL colors
pub fn rgb_to_hsl(red: u8, green: u8, blue: u8) -> (f32, f32, f32) {
    let red = red as f32 / 255.;
    let green = green as f32 / 255.;
    let blue = blue as f32 / 255.;

    let max = if red > green && red > blue {
        red
    } else if green > blue {
        green
    } else {
        blue
    };

    let min = if red < green && red < blue {
        red
    } else if green < blue {
        green
    } else {
        blue
    };

    let lightness = (max + min) / 2.;

    let saturation = if (max - min).abs() < f32::EPSILON {
        0.
    } else if lightness < 0.5 {
        (max - min) / (max + min)
    } else {
        (max - min) / (2. - (max + min))
    };

    let mut hue = if (max - red).abs() < f32::EPSILON {
        60. * (green - blue) / (max - min)
    } else if (max - green).abs() < f32::EPSILON {
        120. + 60. * (blue - red) / (max - min)
    } else {
        240. + 60. * (red - green) / (max - min)
    };

    if hue.is_nan() {
        hue = 0.;
    }

    if hue < 0. {
        hue += 360.;
    }

    (hue, (saturation * 100.), (lightness * 100.))
}

/// Convert HSL colors to RGB colors
pub fn hsl_to_rgb(hue: f32, saturation: f32, lightness: f32) -> (u8, u8, u8) {
    let saturation = saturation as f32 / 100.;
    let lightness = lightness as f32 / 100.;

    let c = (1. - (2. * lightness - 1.).abs()) * saturation;
    let x = c * (1. - ((hue / 60.) % 2. - 1.).abs());
    let m = lightness - c / 2.;

    let (red, green, blue) = match hue {
        _ if hue < 60. => (c, x, 0.),
        _ if hue < 120. => (x, c, 0.),
        _ if hue < 180. => (0., c, x),
        _ if hue < 240. => (0., x, c),
        _ if hue < 300. => (x, 0., c),
        _ if hue < 360. => (c, 0., x),
        _ => (0., 0., 0.),
    };

    (
        ((red + m) * 255.).round() as u8,
        ((green + m) * 255.).round() as u8,
        ((blue + m) * 255.).round() as u8,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    fn round(val: (f32, f32, f32)) -> (f32, f32, f32) {
        (val.0.round(), val.1.round(), val.2.round())
    }

    #[test]
    fn convert() {
        assert_eq!(round(rgb_to_hsl(0, 0, 0)), (0., 0., 0.));
        assert_eq!(round(rgb_to_hsl(255, 255, 255)), (0., 0., 100.));
        assert_eq!(round(rgb_to_hsl(255, 0, 0)), (0., 100., 50.));
        assert_eq!(round(rgb_to_hsl(0, 255, 0)), (120., 100., 50.));
        assert_eq!(round(rgb_to_hsl(0, 0, 255)), (240., 100., 50.));
        assert_eq!(round(rgb_to_hsl(255, 255, 0)), (60., 100., 50.));
        assert_eq!(round(rgb_to_hsl(0, 255, 255)), (180., 100., 50.));
        assert_eq!(round(rgb_to_hsl(255, 0, 255)), (300., 100., 50.));
        assert_eq!(round(rgb_to_hsl(191, 191, 191)), (0., 0., 75.));
        assert_eq!(round(rgb_to_hsl(128, 128, 128)), (0., 0., 50.));
        assert_eq!(round(rgb_to_hsl(128, 0, 0)), (0., 100., 25.));
        assert_eq!(round(rgb_to_hsl(128, 128, 0)), (60., 100., 25.));
        assert_eq!(round(rgb_to_hsl(0, 128, 0)), (120., 100., 25.));
        assert_eq!(round(rgb_to_hsl(128, 0, 128)), (300., 100., 25.));
        assert_eq!(round(rgb_to_hsl(0, 128, 128)), (180., 100., 25.));
        assert_eq!(round(rgb_to_hsl(0, 0, 128)), (240., 100., 25.));

        assert_eq!(hsl_to_rgb(0., 0., 0.), (0, 0, 0));
        assert_eq!(hsl_to_rgb(0., 0., 100.), (255, 255, 255));
        assert_eq!(hsl_to_rgb(0., 100., 50.), (255, 0, 0));
        assert_eq!(hsl_to_rgb(120., 100., 50.), (0, 255, 0));
        assert_eq!(hsl_to_rgb(240., 100., 50.), (0, 0, 255));
        assert_eq!(hsl_to_rgb(60., 100., 50.), (255, 255, 0));
        assert_eq!(hsl_to_rgb(180., 100., 50.), (0, 255, 255));
        assert_eq!(hsl_to_rgb(300., 100., 50.), (255, 0, 255));
        assert_eq!(hsl_to_rgb(0., 0., 75.), (191, 191, 191));
        assert_eq!(hsl_to_rgb(0., 0., 50.), (128, 128, 128));
        assert_eq!(hsl_to_rgb(0., 100., 25.), (128, 0, 0));
        assert_eq!(hsl_to_rgb(60., 100., 25.), (128, 128, 0));
        assert_eq!(hsl_to_rgb(120., 100., 25.), (0, 128, 0));
        assert_eq!(hsl_to_rgb(300., 100., 25.), (128, 0, 128));
        assert_eq!(hsl_to_rgb(180., 100., 25.), (0, 128, 128));
        assert_eq!(hsl_to_rgb(240., 100., 25.), (0, 0, 128));
    }
}
