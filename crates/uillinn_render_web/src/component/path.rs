use super::{empty_if_default, format_id};
use uillinn_core::prelude::*;

pub fn render(def: &Path, layout: &str, id: Option<String>) -> String {
    format!(
        r#"<svg{} viewBox="0 0 {} {}" preserveAspectRatio="none" style="{}"><path d="{}" vector-effect="non-scaling-stroke"{} /></svg>"#,
        format_id(&id),
        def.bounding_box.0,
        def.bounding_box.1,
        layout,
        def.to_svg_path(),
        match format!(
            "{}{}{}",
            empty_if_default(def.fill_color, Black, "fill:{};"),
            empty_if_default(def.stroke_color, Transparent, "stroke:{};"),
            empty_if_default(def.stroke_size, 1, "stroke-width:{}px;"),
        )
        .as_str()
        {
            "" => "".to_string(),
            styles => format!(r#" style="{}""#, styles),
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use uillinn_core::component::CustomComponent;

    #[test]
    fn render_default_path() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.);

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
            ),
            vec![
                r#"<svg viewBox="0 0 20 20" preserveAspectRatio="none" style="left:0px;top:0px;width:100%;height:100%;">"#,
                r#"<path d="m20,0 l0,20 l-20,0 l20,-20" vector-effect="non-scaling-stroke" style="stroke:rgb(0,0,0);stroke-width:0px;" />"#,
                r#"</svg>"#,
            ].join("")
        );
    }

    #[test]
    fn render_path_with_fill_color() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .fill_color(Red)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
            ),
            vec![
                r#"<svg viewBox="0 0 20 20" preserveAspectRatio="none" style="left:0px;top:0px;width:100%;height:100%;">"#,
                r##"<path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" style="fill:rgb(255,65,54);stroke:rgb(0,0,0);stroke-width:0px;" />"##,
                r#"</svg>"#,
            ].join("")
        );
    }

    #[test]
    fn render_path_with_stroke_color() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .stroke_color(Red)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
            ),
            vec![
                r#"<svg viewBox="0 0 20 20" preserveAspectRatio="none" style="left:0px;top:0px;width:100%;height:100%;">"#,
                r##"<path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" style="stroke:rgb(255,65,54);stroke-width:0px;" />"##,
                r#"</svg>"#,
            ].join("")
        );
    }

    #[test]
    fn render_path_with_stroke_size() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .stroke_color(Blue)
            .stroke_size(3)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
            ),
            vec![
                r#"<svg viewBox="0 0 20 20" preserveAspectRatio="none" style="left:0px;top:0px;width:100%;height:100%;">"#,
                r##"<path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" style="stroke:rgb(0,116,217);stroke-width:3px;" />"##,
                r#"</svg>"#,
            ].join("")
        );
    }
}
