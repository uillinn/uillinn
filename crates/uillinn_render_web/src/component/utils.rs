use std::fmt;

// TODO: Shared with `uillinn_render_svg`, create another crate?
pub fn empty_if_default<T: PartialEq + fmt::Display>(
    val: T,
    default: T,
    formatting: &'static str,
) -> String {
    if val == default {
        String::new()
    } else {
        formatting.replace("{}", val.to_string().as_str())
    }
}

/// Format an ID
pub fn format_id(id: &Option<String>) -> String {
    if let Some(id) = id {
        format!(r#" class="{}""#, id)
    } else {
        String::new()
    }
}
