pub(crate) mod ellipse;
pub(crate) mod interactive;
pub(crate) mod paragraph;
pub(crate) mod path;
pub(crate) mod rectangle;
pub(crate) mod text;
pub(crate) mod utils;

use utils::*;

pub const WILL_BE_REPLACED_BY_HASH: &str = "--WILL_BE_REPLACED_BY_HASH--";
pub const DEFAULT_STYLES: &str = "*{\
position:absolute;\
box-sizing:border-box;\
overflow:hidden;\
}\
div{\
border-style:solid;\
border-width:0;\
}\
p{\
margin:0;\
color:#000;\
font-size:16px;\
line-height:1.2;\
font-family:serif;\
text-overflow:ellipsis;\
text-align:left;\
white-space: pre-line;\
}\
span{\
color:#000;\
font-size:16px;\
font-family:sans-serif;\
white-space:nowrap;\
text-align:center;\
}\
input {\
appearance:none;\
-moz-appearance:none;\
-webkit-appearance:none;\
outline:none;\
background:transparent;\
margin:0;\
padding:0;\
border:0;\
color:#000;\
font-size:16px;\
font-family:sans-serif;\
}\
input::placeholder {\
opacity:1;\
}";

pub const DEFAULT_SCRIPT: &str = r#"
<script async deref>
/* The following code is a modified version of `morphdom`, available at
 * https://github.com/patrick-steele-idem/morphdom, under the MIT license
 * (https://github.com/patrick-steele-idem/morphdom/blob/master/LICENSE)
 */
var DOCUMENT_FRAGMENT_NODE = 11;
var ELEMENT_NODE = 1;
var TEXT_NODE = 3;
var COMMENT_NODE = 8;

var range;
var NS_XHTML = "http://www.w3.org/1999/xhtml";

var doc = typeof document === "undefined" ? undefined : document;
var HAS_TEMPLATE_SUPPORT = !!doc && "content" in doc.createElement("template");
var HAS_RANGE_SUPPORT = !!doc && doc.createRange && "createContextualFragment" in doc.createRange();

function morphAttrs(fromNode, toNode) {
    var toNodeAttrs = toNode.attributes;
    var attr;
    var attrName;
    var attrNamespaceURI;
    var attrValue;
    var fromValue;

    for (var i = toNodeAttrs.length - 1; i >= 0; i--) {
        attr = toNodeAttrs[i];
        attrName = attr.name;
        attrNamespaceURI = attr.namespaceURI;
        attrValue = attr.value;

        if (attrNamespaceURI) {
            attrName = attr.localName || attrName;
            fromValue = fromNode.getAttributeNS(attrNamespaceURI, attrName);

            if (fromValue !== attrValue) {
                if (attr.prefix === "xmlns"){
                    attrName = attr.name;
                }
                fromNode.setAttributeNS(attrNamespaceURI, attrName, attrValue);
            }
        } else {
            fromValue = fromNode.getAttribute(attrName);

            if (fromValue !== attrValue) {
                fromNode.setAttribute(attrName, attrValue);
            }
        }
    }

    var fromNodeAttrs = fromNode.attributes;

    for (var d = fromNodeAttrs.length - 1; d >= 0; d--) {
        attr = fromNodeAttrs[d];
        attrName = attr.name;
        attrNamespaceURI = attr.namespaceURI;

        if (attrNamespaceURI) {
            attrName = attr.localName || attrName;

            if (!toNode.hasAttributeNS(attrNamespaceURI, attrName)) {
                fromNode.removeAttributeNS(attrNamespaceURI, attrName);
            }
        } else {
            if (!toNode.hasAttribute(attrName)) {
                fromNode.removeAttribute(attrName);
            }
        }
    }
}

function compareNodeNames(fromEl, toEl) {
    var fromNodeName = fromEl.nodeName;
    var toNodeName = toEl.nodeName;
    var fromCodeStart, toCodeStart;

    if (fromNodeName === toNodeName) {
        return true;
    }

    fromCodeStart = fromNodeName.charCodeAt(0);
    toCodeStart = toNodeName.charCodeAt(0);

    if (fromCodeStart <= 90 && toCodeStart >= 97) {
        return fromNodeName === toNodeName.toUpperCase();
    } else if (toCodeStart <= 90 && fromCodeStart >= 97) {
        return toNodeName === fromNodeName.toUpperCase();
    } else {
        return false;
    }
}

function syncBooleanAttrProp(fromEl, toEl, name) {
    if (fromEl[name] !== toEl[name]) {
        fromEl[name] = toEl[name];
        if (fromEl[name]) {
            fromEl.setAttribute(name, "");
        } else {
            fromEl.removeAttribute(name);
        }
    }
}

var specialElHandlers = {
    OPTION: function(fromEl, toEl) {
        var parentNode = fromEl.parentNode;
        if (parentNode) {
            var parentName = parentNode.nodeName.toUpperCase();
            if (parentName === "OPTGROUP") {
                parentNode = parentNode.parentNode;
                parentName = parentNode && parentNode.nodeName.toUpperCase();
            }
            if (parentName === "SELECT" && !parentNode.hasAttribute("multiple")) {
                if (fromEl.hasAttribute("selected") && !toEl.selected) {
                    fromEl.setAttribute("selected", "selected");
                    fromEl.removeAttribute("selected");
                }
                parentNode.selectedIndex = -1;
            }
        }
        syncBooleanAttrProp(fromEl, toEl, "selected");
    },
    INPUT: function(fromEl, toEl) {
        syncBooleanAttrProp(fromEl, toEl, "checked");
        syncBooleanAttrProp(fromEl, toEl, "disabled");

        if (fromEl.value !== toEl.value) {
            fromEl.value = toEl.value;
        }

        if (!toEl.hasAttribute("value")) {
            fromEl.removeAttribute("value");
        }
    },

    TEXTAREA: function(fromEl, toEl) {
        var newValue = toEl.value;
        if (fromEl.value !== newValue) {
            fromEl.value = newValue;
        }

        var firstChild = fromEl.firstChild;
        if (firstChild) {
            var oldValue = firstChild.nodeValue;

            if (oldValue == newValue || (!newValue && oldValue == fromEl.placeholder)) {
                return;
            }

            firstChild.nodeValue = newValue;
        }
    },
    SELECT: function(fromEl, toEl) {
        if (!toEl.hasAttribute("multiple")) {
            var selectedIndex = -1;
            var i = 0;
            var curChild = fromEl.firstChild;
            var optgroup;
            var nodeName;
            while(curChild) {
                nodeName = curChild.nodeName && curChild.nodeName.toUpperCase();
                if (nodeName === "OPTGROUP") {
                    optgroup = curChild;
                    curChild = optgroup.firstChild;
                } else {
                    if (nodeName === "OPTION") {
                        if (curChild.hasAttribute("selected")) {
                            selectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    curChild = curChild.nextSibling;
                    if (!curChild && optgroup) {
                        curChild = optgroup.nextSibling;
                        optgroup = null;
                    }
                }
            }

            fromEl.selectedIndex = selectedIndex;
        }
    }
};

function getNodeKey(node) {
    if (node) {
        return (node.getAttribute && node.getAttribute("id")) || node.id;
    }
}

function morphdom(fromNode, toNodeHtml) {
    var toNode = doc.createElement("html");
    toNode.innerHTML = toNodeHtml;
    var fromNodesLookup = Object.create(null);
    var keyedRemovalList = [];

    function addKeyedRemoval(key) {
        keyedRemovalList.push(key);
    }

    function walkDiscardedChildNodes(node, skipKeyedNodes) {
        if (node.nodeType === ELEMENT_NODE) {
            var curChild = node.firstChild;
            while (curChild) {
                var key = undefined;

                if (skipKeyedNodes && (key = getNodeKey(curChild))) {
                    addKeyedRemoval(key);
                } else {
                    if (curChild.firstChild) {
                        walkDiscardedChildNodes(curChild, skipKeyedNodes);
                    }
                }

                curChild = curChild.nextSibling;
            }
        }
    }

    function removeNode(node, parentNode, skipKeyedNodes) {
        if (parentNode) {
            parentNode.removeChild(node);
        }

        walkDiscardedChildNodes(node, skipKeyedNodes);
    }

    function indexTree(node) {
        if (node.nodeType === ELEMENT_NODE || node.nodeType === DOCUMENT_FRAGMENT_NODE) {
            var curChild = node.firstChild;
            while (curChild) {
                var key = getNodeKey(curChild);
                if (key) {
                    fromNodesLookup[key] = curChild;
                }

                indexTree(curChild);

                curChild = curChild.nextSibling;
            }
        }
    }

    indexTree(fromNode);

    function handleNodeAdded(el) {
        var curChild = el.firstChild;
        while (curChild) {
            var nextSibling = curChild.nextSibling;

            var key = getNodeKey(curChild);
            if (key) {
                var unmatchedFromEl = fromNodesLookup[key];
                if (unmatchedFromEl && compareNodeNames(curChild, unmatchedFromEl)) {
                    curChild.parentNode.replaceChild(unmatchedFromEl, curChild);
                    morphEl(unmatchedFromEl, curChild);
                } else {
                    handleNodeAdded(curChild);
                }
            } else {
                handleNodeAdded(curChild);
            }

            curChild = nextSibling;
        }
    }

    function cleanupFromEl(fromEl, curFromNodeChild, curFromNodeKey) {
        while (curFromNodeChild) {
            var fromNextSibling = curFromNodeChild.nextSibling;
            if ((curFromNodeKey = getNodeKey(curFromNodeChild))) {
                addKeyedRemoval(curFromNodeKey);
            } else {
                removeNode(curFromNodeChild, fromEl, true);
            }
            curFromNodeChild = fromNextSibling;
        }
    }

    function morphEl(fromEl, toEl) {
        var toElKey = getNodeKey(toEl);

        if (toElKey) {
            delete fromNodesLookup[toElKey];
        }

        morphAttrs(fromEl, toEl);

        if (fromEl.nodeName !== "TEXTAREA") {
            morphChildren(fromEl, toEl);
        } else {
            specialElHandlers.TEXTAREA(fromEl, toEl);
        }
    }

    function morphChildren(fromEl, toEl) {
        var curToNodeChild = toEl.firstChild;
        var curFromNodeChild = fromEl.firstChild;
        var curToNodeKey;
        var curFromNodeKey;

        var fromNextSibling;
        var toNextSibling;
        var matchingFromEl;

        outer: while (curToNodeChild) {
            toNextSibling = curToNodeChild.nextSibling;
            curToNodeKey = getNodeKey(curToNodeChild);

            while (curFromNodeChild) {
                fromNextSibling = curFromNodeChild.nextSibling;

                if (curToNodeChild.isSameNode && curToNodeChild.isSameNode(curFromNodeChild)) {
                    curToNodeChild = toNextSibling;
                    curFromNodeChild = fromNextSibling;
                    continue outer;
                }

                curFromNodeKey = getNodeKey(curFromNodeChild);

                var curFromNodeType = curFromNodeChild.nodeType;

                var isCompatible = undefined;

                if (curFromNodeType === curToNodeChild.nodeType) {
                    if (curFromNodeType === ELEMENT_NODE) {
                        if (curToNodeKey) {
                            if (curToNodeKey !== curFromNodeKey) {
                                if ((matchingFromEl = fromNodesLookup[curToNodeKey])) {
                                    if (fromNextSibling === matchingFromEl) {
                                        isCompatible = false;
                                    } else {
                                        fromEl.insertBefore(matchingFromEl, curFromNodeChild);
                                        if (curFromNodeKey) {
                                            addKeyedRemoval(curFromNodeKey);
                                        } else {
                                            removeNode(curFromNodeChild, fromEl, true);
                                        }

                                        curFromNodeChild = matchingFromEl;
                                    }
                                } else {
                                    isCompatible = false;
                                }
                            }
                        } else if (curFromNodeKey) {
                            isCompatible = false;
                        }

                        isCompatible = isCompatible !== false && compareNodeNames(curFromNodeChild, curToNodeChild);
                        if (isCompatible) {
                            morphEl(curFromNodeChild, curToNodeChild);
                        }

                    } else if (curFromNodeType === TEXT_NODE || curFromNodeType == COMMENT_NODE) {
                        isCompatible = true;
                        if (curFromNodeChild.nodeValue !== curToNodeChild.nodeValue) {
                            curFromNodeChild.nodeValue = curToNodeChild.nodeValue;
                        }

                    }
                }

                if (isCompatible) {
                    curToNodeChild = toNextSibling;
                    curFromNodeChild = fromNextSibling;
                    continue outer;
                }

                if (curFromNodeKey) {
                    addKeyedRemoval(curFromNodeKey);
                } else {
                    removeNode(curFromNodeChild, fromEl, true);
                }

                curFromNodeChild = fromNextSibling;
            }

            if (curToNodeKey && (matchingFromEl = fromNodesLookup[curToNodeKey]) && compareNodeNames(matchingFromEl, curToNodeChild)) {
                fromEl.appendChild(matchingFromEl);
                morphEl(matchingFromEl, curToNodeChild);
            } else {
                if (curToNodeChild.actualize) {
                    curToNodeChild = curToNodeChild.actualize(fromEl.ownerDocument || doc);
                }
                fromEl.appendChild(curToNodeChild);
                handleNodeAdded(curToNodeChild);
            }

            curToNodeChild = toNextSibling;
            curFromNodeChild = fromNextSibling;
        }

        cleanupFromEl(fromEl, curFromNodeChild, curFromNodeKey);

        var specialElHandler = specialElHandlers[fromEl.nodeName];
        if (specialElHandler) {
            specialElHandler(fromEl, toEl);
        }
    }

    var morphedNode = fromNode;
    var morphedNodeType = morphedNode.nodeType;
    var toNodeType = toNode.nodeType;

    if (morphedNode !== toNode) {
        if (toNode.isSameNode && toNode.isSameNode(morphedNode)) {
            return;
        }

        morphEl(morphedNode, toNode);

        if (keyedRemovalList) {
            for (var i=0, len=keyedRemovalList.length; i<len; i++) {
                var elToRemove = fromNodesLookup[keyedRemovalList[i]];
                if (elToRemove) {
                    removeNode(elToRemove, elToRemove.parentNode, false);
                }
            }
        }
    }

    return morphedNode;
}

function onCustomFormSubmit(e) {
    e.preventDefault();
    const body = Array.from(e.target.elements).map((el) => { return encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value == null ? "" : el.value); }).join("&");
    const request = new Request(e.target.method == "post" ? e.target.action : e.target.action + "?" + body, e.target.method == "post" ? {
        method: "POST",
        body,
    } : {
        method: "GET",
    });
    fetch(request)
    .then(response => response.text())
    .then(content => {
        morphdom(document.querySelector("html"), content);
        console.log(document.querySelectorAll("form"));
        console.log(document.querySelectorAll("form:not(.js-enhanced)"));
        document.querySelectorAll("form:not(.js-enhanced)").forEach((form) => {
            customFormSubmit(form);
        });
    });
}

function customFormSubmit(form) {
    form.removeEventListener("submit", onCustomFormSubmit);
    form.addEventListener("submit", onCustomFormSubmit);
}

document.querySelectorAll("form:not(.js-enhanced)").forEach((form) => {
    customFormSubmit(form);
});
</script>"#;

/// Remove HTML tags from a string
pub fn sanitize_string(val: &str) -> String {
    val.replace('<', "&lt;")
        .replace('>', "&gt;")
        .replace('"', "&quot;")
}
