use super::{empty_if_default, format_id};
use std::f32::consts::PI;
use uillinn_core::prelude::*;

// TODO: Shared with `uillinn_render_svg`, create another crate?
fn polar_to_cartesian(
    center_x: f32,
    center_y: f32,
    radius_x: f32,
    radius_y: f32,
    degrees: usize,
) -> (f32, f32) {
    let radians = (degrees as f32 - 90.) * PI / 180.;

    (
        center_x + (radius_x * f32::cos(radians)),
        center_y + (radius_y * f32::sin(radians)),
    )
}

fn gen_path_data(
    width: f32,
    height: f32,
    start_angle: usize,
    end_angle: usize,
    ratio: usize,
    border_size: usize,
) -> String {
    let ratio_px = ((100. - ratio as f32) / 100.) * (f32::min(width, height) / 2.);
    let center_x = width / 2.;
    let center_y = height / 2.;
    let radius_x = width / 2. - ratio_px - border_size as f32 / 2.;
    let radius_y = height / 2. - ratio_px - border_size as f32 / 2.;

    // If the ellipse is fully filled and there is no border, we need to define a very small stroke around it
    let radius_x = if radius_x == 0. { 0.0001 } else { radius_x };
    let radius_y = if radius_y == 0. { 0.0001 } else { radius_y };

    let start = polar_to_cartesian(center_x, center_y, radius_x, radius_y, end_angle);
    let end = polar_to_cartesian(center_x, center_y, radius_x, radius_y, start_angle);
    let start_border_point = polar_to_cartesian(
        center_x,
        center_y,
        radius_x + ratio_px,
        radius_y + ratio_px,
        start_angle,
    );
    let end_border_point = polar_to_cartesian(
        center_x,
        center_y,
        radius_x + ratio_px,
        radius_y + ratio_px,
        end_angle,
    );

    // TODO: Generate relative points
    if f32::abs(start_angle as f32 - end_angle as f32) > 180. {
        // If the angle of the arc is more than 180 degrees, we need to use two arcs instead of one
        if ratio == 0 {
            // If ratio == 0, we don't need to draw the inner arc

            // If the circle is complete, we need to hide the remaining inner borders. We do that by
            // using the move (`M`) command instead of the line (`L`) command
            let cmd_to_border = if f32::abs(start_angle as f32 - end_angle as f32) == 360. {
                "M"
            } else {
                "L"
            };

            format!(
                "M{} {} {cmd_to_border}{} {}  A{} {} 0 0 1 {} {} A{} {} 0 0 1 {} {} {cmd_to_border}{} {}Z",
                center_x,
                center_y,
                start_border_point.0,
                start_border_point.1,

                radius_x + ratio_px,
                radius_y + ratio_px,
                width / 2.,
                height - border_size as f32 / 2.,

                radius_x + ratio_px,
                radius_y + ratio_px,
                end_border_point.0,
                end_border_point.1,
                center_x,
                center_y,
                cmd_to_border = cmd_to_border,
            )
        } else {
            // If the circle is complete, we need to hide the remaining inner borders. We do that by
            // using the move (`M`) command instead of the line (`L`) command
            let cmd_to_border = if f32::abs(start_angle as f32 - end_angle as f32) == 360. {
                "M"
            } else {
                "L"
            };

            format!(
                "M{} {} A{} {} 0 0 0 {} {} A{} {} 0 0 0 {} {} {cmd_to_border}{} {} A{} {} 0 0 1 {} {} A{} {} 0 0 1 {} {} {cmd_to_border}{} {}Z",
                start.0,
                start.1,

                radius_x,
                radius_y,
                width / 2.,
                height - ratio_px - border_size as f32 / 2.,

                radius_x,
                radius_y,
                end.0,
                end.1,

                start_border_point.0,
                start_border_point.1,

                radius_x + ratio_px,
                radius_y + ratio_px,
                width / 2.,
                height - border_size as f32 / 2.,

                radius_x + ratio_px,
                radius_y + ratio_px,
                end_border_point.0,
                end_border_point.1,

                start.0,
                start.1,
                cmd_to_border = cmd_to_border,
            )
        }
    } else if ratio == 0 {
        // If ratio == 0, we don't need to draw the inner arc
        format!(
            "M{} {} L{} {} A{} {} 0 0 1 {} {} L{} {}Z",
            center_x,
            center_y,
            start_border_point.0,
            start_border_point.1,
            radius_x + ratio_px,
            radius_y + ratio_px,
            end_border_point.0,
            end_border_point.1,
            center_x,
            center_y,
        )
    } else {
        format!(
            "M{} {} A{} {} 0 0 0 {} {} L{} {} A{} {} 0 0 1 {} {} L{} {}Z",
            start.0,
            start.1,
            radius_x,
            radius_y,
            end.0,
            end.1,
            start_border_point.0,
            start_border_point.1,
            radius_x + ratio_px,
            radius_y + ratio_px,
            end_border_point.0,
            end_border_point.1,
            start.0,
            start.1,
        )
    }
}

pub fn render(def: &Ellipse, layout: &str, id: Option<String>, area: &Area) -> String {
    format!(
        r#"<svg{} style="{}{}"><path d="{}" fill-rule="evenodd"{} /></svg>"#,
        format_id(&id),
        layout,
        empty_if_default(def.rotation, 0, "transform:rotate({}deg);"),
        gen_path_data(
            match area.width.default {
                Px(val) => val as f32,
                Percent(_val) => todo!(),
            },
            match area.height.default {
                Px(val) => val as f32,
                Percent(_val) => todo!(),
            },
            def.start_angle,
            def.end_angle,
            def.ratio,
            def.border_size
        ),
        match format!(
            "{}{}{}",
            empty_if_default(def.color, Black, "fill:{};"),
            empty_if_default(def.border_color, Transparent, "stroke:{};"),
            empty_if_default(def.border_size, 1, "stroke-width:{};"),
        )
        .as_str()
        {
            "" => "".to_string(),
            styles => format!(r#" style="{}""#, styles),
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_ellipse() {
        let component = Ellipse::new();

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 50 M49.999996 -0.00009918213  A50.0001 50.0001 0 0 1 50 100 A50.0001 50.0001 0 0 1 50 -0.00009918213 M50 50Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_ratio() {
        let component = Ellipse::new().ratio(20);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 40 A10 10 0 0 0 50 60 A10 10 0 0 0 50 40 M49.999996 0 A50 50 0 0 1 50 100 A50 50 0 0 1 50 0 M50 40Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_start_angle() {
        let component = Ellipse::new().start_angle(90);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 50 L100.0001 50  A50.0001 50.0001 0 0 1 50 100 A50.0001 50.0001 0 0 1 50 -0.00009918213 L50 50Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_end_angle() {
        let component = Ellipse::new().end_angle(180);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 50 L49.999996 -0.00009918213 A50.0001 50.0001 0 0 1 49.999996 100.0001 L50 50Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_default_ellipse_mixed() {
        let component = Ellipse::new().ratio(90).start_angle(90).end_angle(270);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M5 49.999996 A45 45 0 0 0 95 50 L100 50 A50 50 0 0 1 0 49.999996 L5 49.999996Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_color() {
        let component = Ellipse::new().color(Rgb(12, 12, 33));

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 50 M49.999996 -0.00009918213  A50.0001 50.0001 0 0 1 50 100 A50.0001 50.0001 0 0 1 50 -0.00009918213 M50 50Z" fill-rule="evenodd" style="fill:rgb(12,12,33);stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_borders() {
        let component = Ellipse::new().border_size(20).border_color(Red);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;"><path d="M50 50 M50 10  A40 40 0 0 1 50 90 A40 40 0 0 1 50 10 M50 50Z" fill-rule="evenodd" style="stroke:rgb(255,65,54);stroke-width:20;" /></svg>"#,
        );
    }

    #[test]
    fn render_ellipse_with_rotation() {
        let component = Ellipse::new().rotate(20);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &Area::new(0, 0, 100, 100),
            ),
            r#"<svg style="left:0px;top:0px;width:100%;height:100%;transform:rotate(20deg);"><path d="M50 50 M49.999996 -0.00009918213  A50.0001 50.0001 0 0 1 50 100 A50.0001 50.0001 0 0 1 50 -0.00009918213 M50 50Z" fill-rule="evenodd" style="stroke:rgb(0,0,0);stroke-width:0;" /></svg>"#,
        );
    }
}
