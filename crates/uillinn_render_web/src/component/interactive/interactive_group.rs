use super::format_id;
use uillinn_core::prelude::*;

pub fn render(
    def: &InteractiveGroup,
    layout: &str,
    id: Option<String>,
    additional_styles: &mut String,
) -> String {
    // Compute the styles of the trigger component
    // `.unwrap()` won't fire an error because an `InteractiveGroup` must have a trigger component
    // when it is built
    let trigger_area = &def.trigger.as_ref().unwrap().0;
    let trigger_component = &def.trigger.as_ref().unwrap().1;
    let trigger_id = trigger_component.get_hash(trigger_area);
    let (trigger_layout, trigger_computed_additional_styles, _) =
        crate::calc_layout(trigger_area, trigger_component, Some(trigger_id.clone()));
    additional_styles.push_str(trigger_computed_additional_styles.as_str());

    format!(
        r#"<form{} method="{}" action="{}" style="{}">{}<input type="submit" id="trigger-{}" style="opacity:0;{}"><label for="trigger-{}" style="cursor:pointer;{}">{}</label></form>"#,
        format_id(&id),
        def.action.kind,
        def.action.path,
        layout,
        def.children
            .iter()
            .map(
                |(name, is_required, (area, component))| match component.kind {
                    ComponentKind::Interactive(ref kind) => {
                        // Render interactive components using the name of the child
                        let (layout, computed_additional_styles, id) =
                            crate::calc_layout(area, component, None);
                        additional_styles.push_str(computed_additional_styles.as_str());

                        super::render(
                            Some(name),
                            is_required,
                            kind,
                            &layout,
                            id,
                            additional_styles,
                        )
                    }
                    _ => unreachable!(),
                }
            )
            .collect::<String>(),
        trigger_id,
        trigger_layout,
        trigger_id,
        trigger_layout,
        crate::render_to_string(None, trigger_component, additional_styles),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_interactive_group_with_interactive_components() {
        let text_input = TextInput::new();
        let group = InteractiveGroup::new(GET, "/new")
            .child(
                "my-input",
                Required,
                Area::new(0, 0, Percent(100), Percent(100)),
                text_input,
            )
            .child_trigger(Area::new(10, 10, 10, 10), Rectangle::new().color(Blue));

        assert_eq!(
            render(
                &group,
                "left:0px;top:0px;width:100%;height:100%;",
                None,
                &mut String::new()
            ),
            r#"<form method="GET" action="/new" style="left:0px;top:0px;width:100%;height:100%;"><input name="my-input" type="text" placeholder="Enter some text…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;" required><input type="submit" id="trigger-ui-36a53f6a2b9e6bac" style="opacity:0;left:10px;top:10px;width:10px;height:10px;"><label for="trigger-ui-36a53f6a2b9e6bac" style="cursor:pointer;left:10px;top:10px;width:10px;height:10px;"><div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,116,217);"></div></label></form>"#,
        );
    }
}
