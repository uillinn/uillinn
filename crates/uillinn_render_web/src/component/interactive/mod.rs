pub mod boolean_input;
pub mod interactive_group;
pub mod text_input;

use super::{empty_if_default, format_id};
use uillinn_core::prelude::*;

pub fn render(
    name: Option<&str>,
    is_required: &IsRequired,
    kind: &InteractiveComponentKind,
    layout: &str,
    id: Option<String>,
    additional_styles: &mut String,
) -> String {
    match kind {
        InteractiveComponentKind::TextInput(ref def) => {
            text_input::render(name, is_required, def, layout, id)
        }
        InteractiveComponentKind::BooleanInput(ref def) => {
            boolean_input::render(name, is_required, def, layout, id, additional_styles)
        }
        InteractiveComponentKind::InteractiveGroup(ref def) => {
            interactive_group::render(def, layout, id, additional_styles)
        }
    }
}
