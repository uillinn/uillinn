use uillinn_core::prelude::*;

pub fn render(
    name: Option<&str>,
    is_required: &IsRequired,
    def: &BooleanInput,
    layout: &str,
    id: Option<String>,
    additional_styles: &mut String,
) -> String {
    // According to `uillinn_render_web/src/lib.rs:137`, `id` cannot be `None`
    let id = id.unwrap();

    // input#{id}:checked ~ label[for="{id}"]:last-of-type{{display:none !important;}}
    additional_styles.push_str(format!(r#"input#{id}:checked ~ label[for="{id}"].true-case{{display:block !important;}}input#{id}:checked ~ label[for="{id}"].false-case{{display:none !important;}}"#, id = id).as_str());

    format!(
        r#"<input id="{id}"{}{} type="checkbox" style="opacity:0;"{}><label for="{id}" class="false-case" style="cursor:pointer;{}">{}</label><label for="{id}" class="true-case" style="display:none;cursor:pointer;{}">{}</label>"#,
        if let Some(ref name) = name {
            format!(r#" name="{}""#, name)
        } else {
            String::new()
        },
        if def.is_checked_by_default {
            " checked"
        } else {
            ""
        },
        match is_required {
            Required => " required".to_string(),
            NotRequired(default) =>
                if default.is_empty() {
                    "".to_string()
                } else {
                    format!(r#" value="{}""#, default)
                },
        },
        layout,
        crate::render_to_string(None, &def.false_case, additional_styles),
        layout,
        crate::render_to_string(None, &def.true_case, additional_styles),
        id = id,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_boolean_input() {
        let component =
            BooleanInput::new(Rectangle::new().color(Blue), Rectangle::new().color(Red));
        let mut additional_styles = String::new();

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                Some("ui-aaabbbcccddd".to_string()),
                &mut additional_styles,
            ),
            r#"<input id="ui-aaabbbcccddd" type="checkbox" style="opacity:0;"><label for="ui-aaabbbcccddd" class="false-case" style="cursor:pointer;left:0px;top:0px;width:100%;height:100%;"><div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);"></div></label><label for="ui-aaabbbcccddd" class="true-case" style="display:none;cursor:pointer;left:0px;top:0px;width:100%;height:100%;"><div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,116,217);"></div></label>"#,
        );

        assert_eq!(additional_styles, r#"input#ui-aaabbbcccddd:checked ~ label[for="ui-aaabbbcccddd"].true-case{display:block !important;}input#ui-aaabbbcccddd:checked ~ label[for="ui-aaabbbcccddd"].false-case{display:none !important;}"#.to_string());
    }
}
