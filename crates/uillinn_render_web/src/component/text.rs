use super::{empty_if_default, format_id, sanitize_string};
use crate::layout::Unit;
use uillinn_core::{layout::ScreenBasedKind, prelude::*};

pub fn render(def: &Text, layout: &str, id: Option<String>, height: &ScreenBased<Size>) -> String {
    format!(
        r#"<span{} style="{}{}{}{}{}{}{}{}">{}</span>{}"#,
        format_id(&id),
        layout,
        match height.default {
            Px(val) => format!("line-height:{}px;", val),
            Percent(_) => format!(
                "display:flex;align-items:center;justify-content:{};",
                match def.align {
                    Alignment::Left => "flex-start",
                    Alignment::Center => "center",
                    Alignment::Right => "flex-end",
                }
            ), // Fallback to Flexbox when the height is specified in percents
        },
        empty_if_default(def.align, Center, "text-align:{};"),
        empty_if_default(def.color, Black, "color:{};"),
        empty_if_default(&def.font_family, &"sans-serif".into(), "font-family:{};"),
        empty_if_default(def.font_size, 16, "font-size:{}px;"),
        empty_if_default(def.font_weight, 400, "font-weight:{};"),
        empty_if_default(def.rotation, 0, "transform:rotate({}deg);"),
        sanitize_string(&def.content),
        if let Percent(_) = height.default {
            "".to_string()
        } else {
            match height
                .breakpoints
                .iter()
                .map(|b| {
                    let (screen_rule_type, breakpoint, val) = match b {
                        ScreenBasedKind::WidthSmallerThan(breakpoint, val) => {
                            ("max-width", breakpoint, val)
                        }
                        ScreenBasedKind::WidthWiderThan(breakpoint, val) => {
                            ("min-width", breakpoint, val)
                        }
                        ScreenBasedKind::HeightSmallerThan(breakpoint, val) => {
                            ("max-height", breakpoint, val)
                        }
                        ScreenBasedKind::HeightWiderThan(breakpoint, val) => {
                            ("min-height", breakpoint, val)
                        }
                    };

                    format!(
                        "@media screen and ({}: {}px){{.{}{{line-height:{}!important;}}}}",
                        screen_rule_type,
                        breakpoint,
                        id.as_ref().unwrap(), // At this point, we are sure that `id` is not None
                        match val {
                            Px(val) => Unit::Px(*val as isize),
                            Percent(val) => Unit::Percent(*val as isize),
                        },
                    )
                })
                .collect::<String>()
                .as_str()
            {
                "" => "".to_string(),
                style => format!("<style>{}</style>", style),
            }
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_text() {
        let component = Text::new("Hello world!");

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_default_text_sanitized() {
        let component = Text::new(r#"Hello world! <script>alert("Hello world!")</script>"#);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;">Hello world! &lt;script&gt;alert(&quot;Hello world!&quot;)&lt;/script&gt;</span>"#,
        );
    }

    #[test]
    fn render_text_with_color() {
        let component = Text::new("Hello world!").color(Red);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;color:rgb(255,65,54);">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_font_family() {
        let component = Text::new("Hello world!").font_family("my-font");

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;font-family:my-font;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_font_family_fallback() {
        let component = Text::new("Hello world!").font_family(vec!["Noto Sans", "sans-serif"]);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;font-family:Noto Sans,sans-serif;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_font_size() {
        let component = Text::new("Hello world!").font_size(30);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;font-size:30px;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_font_weight() {
        let component = Text::new("Hello world!").font_weight(900);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;font-weight:900;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_alignment() {
        let component = Text::new("Hello world!").align(Alignment::Right);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;text-align:right;">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_rotation() {
        let component = Text::new("Hello world!").rotate(30);

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:20px;",
                None,
                &ScreenBased::new(Px(20)),
            ),
            r#"<span style="left:0px;top:0px;width:100%;height:20px;line-height:20px;transform:rotate(30deg);">Hello world!</span>"#,
        );
    }

    #[test]
    fn render_text_with_height_changed_between_screen_sizes() {
        let component = Text::new("Hello world!");

        assert_eq!(
            render(
                &component,
                "left:0px;top:0px;width:100%;height:50px;",
                Some("ui-42abc".to_string()),
                &ScreenBased::new(Px(50)).width_smaller_than(1000, 70),
            ),
            r#"<span class="ui-42abc" style="left:0px;top:0px;width:100%;height:50px;line-height:50px;">Hello world!</span><style>@media screen and (max-width: 1000px){.ui-42abc{line-height:70px!important;}}</style>"#,
        );
    }
}
