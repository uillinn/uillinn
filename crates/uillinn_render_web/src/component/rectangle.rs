use super::{empty_if_default, format_id};
use uillinn_core::prelude::*;

pub fn render(def: &Rectangle, layout: &str, id: Option<String>) -> String {
    format!(
        r#"<div{} style="{}{}{}{}{}{}"></div>"#,
        format_id(&id),
        layout,
        empty_if_default(def.color, Transparent, "background-color:{};"),
        empty_if_default(def.border_size, 0, "border-width:{}px;"),
        empty_if_default(def.border_color, Black, "border-color:{};"),
        if def.rounded == All(0) {
            String::new()
        } else {
            match def.rounded {
                All(ref val) => format!("border-radius:{}px;", val),
                Each(ref v1, ref v2, ref v3, ref v4) => {
                    format!("border-radius:{}px {}px {}px {}px;", v1, v2, v3, v4)
                }
            }
        },
        empty_if_default(def.rotation, 0, "transform:rotate({}deg);"),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_rectangle() {
        let component = Rectangle::new();

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_color() {
        let component = Rectangle::new().color(Red);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_border_size() {
        let component = Rectangle::new().border_size(1);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);border-width:1px;"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_border_color() {
        let component = Rectangle::new().border_size(2).border_color(Red);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);border-width:2px;border-color:rgb(255,65,54);"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rounded_all() {
        let component = Rectangle::new().rounded(20);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);border-radius:20px;"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rounded_each() {
        let component = Rectangle::new().rounded(Each(20, 0, 20, 0));

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);border-radius:20px 0px 20px 0px;"></div>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rotation() {
        let component = Rectangle::new().rotate(90);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,0,0);transform:rotate(90deg);"></div>"#,
        );
    }
}
