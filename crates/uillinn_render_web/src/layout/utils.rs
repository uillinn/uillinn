use crate::WILL_BE_REPLACED_BY_HASH;
use std::fmt;
use uillinn_core::{layout::ScreenBasedKind, prelude::*};

#[derive(Clone)]
pub enum Unit {
    Px(isize),
    Percent(isize),
    Addition(Box<Unit>, Box<Unit>),
}

impl fmt::Display for Unit {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                Unit::Px(val) => val.to_string() + "px",
                Unit::Percent(val) => val.to_string() + "%",
                Unit::Addition(val1, val2) => match (*val1.clone(), *val2.clone()) {
                    (Unit::Px(val1), Unit::Px(val2)) => (val1 + val2).to_string() + "px",
                    (Unit::Px(val1), Unit::Percent(val2)) =>
                        format!("calc({}px + {}%)", val1, val2),
                    (Unit::Percent(val1), Unit::Px(val2)) =>
                        format!("calc({}% + {}px)", val1, val2),
                    (Unit::Percent(val1), Unit::Percent(val2)) => (val1 + val2).to_string() + "%",
                    _ => panic!("Additions must not be recursive"),
                },
            }
        )
    }
}

pub struct LayoutStyles {
    left: Option<Unit>,
    right: Option<Unit>,
    top: Option<Unit>,
    bottom: Option<Unit>,
    width: Option<Unit>,
    height: Option<Unit>,
    transform: (Option<Unit>, Option<Unit>),
    overflow: Option<&'static str>,
    is_important: bool,
}

impl LayoutStyles {
    pub fn new(is_important: bool) -> Self {
        Self {
            left: None,
            right: None,
            top: None,
            bottom: None,
            width: None,
            height: None,
            transform: (None, None),
            overflow: None,
            is_important,
        }
    }

    pub fn size(mut self, size: Size, y_axis: bool) -> Self {
        let prop = if !y_axis {
            &mut self.width
        } else {
            &mut self.height
        };

        match size {
            Px(val) => *prop = Some(Unit::Px(val as isize)),
            Percent(val) => *prop = Some(Unit::Percent(val as isize)),
        }

        self
    }

    pub fn pos(mut self, pos: Pos, y_axis: bool) -> Self {
        let (prop, reversed_prop) = if !y_axis {
            (&mut self.left, &mut self.right)
        } else {
            (&mut self.top, &mut self.bottom)
        };

        match pos {
            StartPx(val) => *prop = Some(Unit::Px(val)),
            StartPercent(val) => *prop = Some(Unit::Percent(val)),
            CenterPx(val) => {
                *prop = if val != 0 {
                    Some(Unit::Addition(
                        Box::new(Unit::Percent(50)),
                        Box::new(Unit::Px(val)),
                    ))
                } else {
                    Some(Unit::Percent(50))
                };

                if !y_axis {
                    self.transform.0 = Some(Unit::Percent(-50));
                } else {
                    self.transform.1 = Some(Unit::Percent(-50));
                }
            }
            CenterPercent(val) => {
                *prop = if val > 0 {
                    Some(Unit::Addition(
                        Box::new(Unit::Percent(50)),
                        Box::new(Unit::Percent(val)),
                    ))
                } else {
                    Some(Unit::Percent(50))
                };

                if !y_axis {
                    self.transform.0 = Some(Unit::Percent(-50));
                } else {
                    self.transform.1 = Some(Unit::Percent(-50));
                }
            }
            EndPx(val) => *reversed_prop = Some(Unit::Px(val)),
            EndPercent(val) => *reversed_prop = Some(Unit::Percent(val)),
        }

        self
    }

    pub fn overflow(mut self, scroll: bool) -> Self {
        if scroll {
            self.overflow = Some("auto");
        } else {
            // `overflow = "hidden"` is the default, nothing to do
        }

        self
    }
}

impl fmt::Display for LayoutStyles {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let important_string = if self.is_important { "!important" } else { "" };
        write!(
            fmt,
            "{}{}{}{}{}{}{}{}",
            if let Some(ref left) = self.left {
                format!("left:{}{};", left, important_string)
            } else {
                "".to_string()
            },
            if let Some(ref right) = self.right {
                format!("right:{}{};", right, important_string)
            } else {
                "".to_string()
            },
            if let Some(ref top) = self.top {
                format!("top:{}{};", top, important_string)
            } else {
                "".to_string()
            },
            if let Some(ref bottom) = self.bottom {
                format!("bottom:{}{};", bottom, important_string)
            } else {
                "".to_string()
            },
            match self.transform {
                (None, None) => "".to_string(),
                (ref x, None) => format!(
                    "transform:translateX({}){};",
                    x.as_ref().unwrap(),
                    important_string
                ),
                (None, ref y) => format!(
                    "transform:translateY({}){};",
                    y.as_ref().unwrap(),
                    important_string
                ),
                (ref x, ref y) => format!(
                    "transform:translateX({})translateY({}){};",
                    x.as_ref().unwrap(),
                    y.as_ref().unwrap(),
                    important_string
                ),
            },
            if let Some(ref width) = self.width {
                format!("width:{}{};", width, important_string)
            } else {
                "".to_string()
            },
            if let Some(ref height) = self.height {
                format!("height:{}{};", height, important_string)
            } else {
                "".to_string()
            },
            if let Some(overflow) = self.overflow {
                format!("overflow:{}{};", overflow, important_string)
            } else {
                "".to_string()
            },
        )
    }
}

/// Format a screen-based position to a CSS media query
///
/// `y_axis` is used to indicate the axis used (if `true`, height, top and bottom will be used
/// instead of width, left and right)
pub fn format_screen_based_pos(
    screen_based_kind: &ScreenBasedKind<Pos>,
    default_styles: &LayoutStyles,
    y_axis: bool,
) -> String {
    let (screen_rule_type, breakpoint, val) = match screen_based_kind {
        ScreenBasedKind::WidthSmallerThan(breakpoint, val) => ("max-width", breakpoint, val),
        ScreenBasedKind::WidthWiderThan(breakpoint, val) => ("min-width", breakpoint, val),
        ScreenBasedKind::HeightSmallerThan(breakpoint, val) => ("max-height", breakpoint, val),
        ScreenBasedKind::HeightWiderThan(breakpoint, val) => ("min-height", breakpoint, val),
    };

    let mut styles = LayoutStyles::new(true).pos(*val, y_axis);

    // If the component is centered by default but not in this screen, we need to reset the
    // `transform` property
    if default_styles.transform.0.is_some() && styles.transform.1.is_none() {
        styles.transform.0 = Some(Unit::Px(0));
    }

    if default_styles.transform.1.is_some() && styles.transform.1.is_none() {
        styles.transform.1 = Some(Unit::Px(0));
    }

    format!(
        "@media screen and ({}: {}px){{.{}{{{}}}}}",
        screen_rule_type, breakpoint, WILL_BE_REPLACED_BY_HASH, styles,
    )
}

/// Format a screen-based size to a CSS media query
///
/// `y_axis` is used to indicate the axis used (if `true`, height, top and bottom will be used
/// instead of width, left and right)
pub fn format_screen_based_size(screen_based_kind: &ScreenBasedKind<Size>, y_axis: bool) -> String {
    let (screen_rule_type, breakpoint, val) = match screen_based_kind {
        ScreenBasedKind::WidthSmallerThan(breakpoint, val) => ("max-width", breakpoint, val),
        ScreenBasedKind::WidthWiderThan(breakpoint, val) => ("min-width", breakpoint, val),
        ScreenBasedKind::HeightSmallerThan(breakpoint, val) => ("max-height", breakpoint, val),
        ScreenBasedKind::HeightWiderThan(breakpoint, val) => ("min-height", breakpoint, val),
    };

    format!(
        "@media screen and ({}: {}px){{.{}{{{}}}}}",
        screen_rule_type,
        breakpoint,
        WILL_BE_REPLACED_BY_HASH,
        LayoutStyles::new(true).size(*val, y_axis),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn display_unit() {
        assert_eq!(format!("{}", Unit::Px(50)), "50px".to_string());
        assert_eq!(format!("{}", Unit::Percent(50)), "50%".to_string());
        assert_eq!(
            format!(
                "{}",
                Unit::Addition(Box::new(Unit::Px(10)), Box::new(Unit::Px(10)))
            ),
            "20px".to_string()
        );
        assert_eq!(
            format!(
                "{}",
                Unit::Addition(Box::new(Unit::Percent(-10)), Box::new(Unit::Percent(20)))
            ),
            "10%".to_string()
        );
        assert_eq!(
            format!(
                "{}",
                Unit::Addition(Box::new(Unit::Px(20)), Box::new(Unit::Percent(10)))
            ),
            "calc(20px + 10%)".to_string()
        );
        assert_eq!(
            format!(
                "{}",
                Unit::Addition(Box::new(Unit::Percent(10)), Box::new(Unit::Px(-30)))
            ),
            "calc(10% + -30px)".to_string()
        );
    }

    #[test]
    #[should_panic(expected = "Additions must not be recursive")]
    fn panic_when_recursive_additions() {
        format!(
            "{}",
            Unit::Addition(
                Box::new(Unit::Addition(
                    Box::new(Unit::Px(20)),
                    Box::new(Unit::Percent(20))
                )),
                Box::new(Unit::Percent(10))
            )
        );
    }
}
