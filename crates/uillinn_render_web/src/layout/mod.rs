use uillinn_core::prelude::*;

mod utils;

pub use utils::*;

pub fn css_layout(area: &Area) -> (String, String) {
    let default_styles = LayoutStyles::new(false)
        .pos(area.x_axis.default, false)
        .pos(area.y_axis.default, true)
        .size(area.width.default, false)
        .size(area.height.default, true)
        .overflow(area.scrollable);

    (
        default_styles.to_string(),
        format!(
            "{}{}{}{}",
            area.x_axis
                .breakpoints
                .iter()
                .map(|kind| format_screen_based_pos(kind, &default_styles, false))
                .collect::<String>(),
            area.y_axis
                .breakpoints
                .iter()
                .map(|kind| format_screen_based_pos(kind, &default_styles, true))
                .collect::<String>(),
            area.width
                .breakpoints
                .iter()
                .map(|kind| format_screen_based_size(kind, false))
                .collect::<String>(),
            area.height
                .breakpoints
                .iter()
                .map(|kind| format_screen_based_size(kind, true))
                .collect::<String>(),
        ),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn css_layout_scroll() {
        assert_eq!(
            css_layout(&Area::new(10, 10, 80, 80).scroll()).0,
            "left:10px;top:10px;width:80px;height:80px;overflow:auto;"
        );
    }

    #[test]
    fn css_layout_default_units() {
        assert_eq!(
            css_layout(&Area::new(10, 10, 80, 80)).0,
            "left:10px;top:10px;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_pos_pixels_left_top() {
        assert_eq!(
            css_layout(&Area::new(StartPx(10), StartPx(10), 80, 80)).0,
            "left:10px;top:10px;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_pos_pixels_right_bottom() {
        assert_eq!(
            css_layout(&Area::new(EndPx(10), EndPx(10), 80, 80)).0,
            "right:10px;bottom:10px;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_pos_percents_left_top() {
        assert_eq!(
            css_layout(&Area::new(StartPercent(10), StartPercent(10), 80, 80)).0,
            "left:10%;top:10%;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_pos_percents_right_bottom() {
        assert_eq!(
            css_layout(&Area::new(EndPercent(10), EndPercent(10), 80, 80)).0,
            "right:10%;bottom:10%;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_pos_center_x() {
        assert_eq!(
            css_layout(&Area::new(CenterPx(0), 0, Percent(80), Percent(50))).0,
            "left:50%;top:0px;transform:translateX(-50%);width:80%;height:50%;"
        );
    }

    #[test]
    fn css_layout_pos_center_y() {
        assert_eq!(
            css_layout(&Area::new(0, CenterPercent(10), Percent(80), Percent(50))).0,
            "left:0px;top:60%;transform:translateY(-50%);width:80%;height:50%;"
        );
    }

    #[test]
    fn css_layout_pos_center_xy() {
        assert_eq!(
            css_layout(&Area::new(CenterPx(0), CenterPx(-100), Percent(80), Percent(50))).0,
            "left:50%;top:calc(50% + -100px);transform:translateX(-50%)translateY(-50%);width:80%;height:50%;"
        );
    }

    #[test]
    fn css_layout_pos_left_top() {
        assert_eq!(
            css_layout(&Area::new(StartPx(0), StartPx(0), Percent(80), Percent(50))).0,
            "left:0px;top:0px;width:80%;height:50%;"
        );
    }

    #[test]
    fn css_layout_pos_right_bottom() {
        assert_eq!(
            css_layout(&Area::new(EndPx(0), EndPx(0), Percent(80), Percent(50))).0,
            "right:0px;bottom:0px;width:80%;height:50%;"
        );
    }

    #[test]
    fn css_layout_size_px() {
        assert_eq!(
            css_layout(&Area::new(10, 10, Px(80), Px(80))).0,
            "left:10px;top:10px;width:80px;height:80px;"
        );
    }

    #[test]
    fn css_layout_size_percents() {
        assert_eq!(
            css_layout(&Area::new(10, 10, Percent(80), Percent(80))).0,
            "left:10px;top:10px;width:80%;height:80%;"
        );
    }

    #[test]
    fn css_layout_size_full_parent() {
        assert_eq!(
            css_layout(&Area::new(10, 10, Percent(100), Percent(100))).0,
            "left:10px;top:10px;width:100%;height:100%;"
        );
    }

    #[test]
    fn css_layout_mixed() {
        assert_eq!(
            css_layout(&Area::new(StartPercent(10), EndPx(10), Percent(80), Px(80))).0,
            "left:10%;bottom:10px;width:80%;height:80px;"
        );
    }
}
