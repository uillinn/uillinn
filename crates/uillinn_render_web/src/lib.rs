//! Render components to a website
//!
//! # Not supported
//!
//! - Using the `ScreenBased` or `Percent` units for the size of `Ellipse`s
//!
//! # Example for generating HTML and CSS in a directory
//!
//! ```rust
//! use uillinn_core::prelude::*;
//! use uillinn_render_web::WebRender;
//!
//! fn main() {
//!     // Start by creating a root component with a red text at (0, 0) (by default the text is centered)
//!     let root = Component::new().child(
//!         Area::new(0, 0, Percent(100), 50),
//!         Text::new("Hello world!").color(Red),
//!     );
//!
//!     // Then, define an `App` containing some metadata about the application (name, description and language of it)
//!     let app = App::new("Hello world!", "Just want to say hello", "en", root);
//!
//!     // Finally, display the HTML code of the application
//!     println!("{}", WebRender::new().render_app(&app));
//! }
//! ```
//!
//! # Example for serving HTML and CSS using a Rust server (with [`vial`](https://vial.rs))
//!
//! ```rust,ignore
//! use uillinn_core::prelude::*;
//! use uillinn_render_web::WebRender;
//! use vial::prelude::*;
//!
//! routes! {
//!     GET "/hello/:name" => hello;
//! }
//!
//! fn hello(req: Request) -> String {
//!     let root = Component::new().child(
//!         Area::new(0, 0, Percent(100), 50),
//!         Text::new(format!("Hello {}!", req.arg("name").unwrap_or("guest"))).color(Red),
//!     );
//!
//!     let app = App::new("Hello world!", "Just want to say hello", "en", root);
//!
//!     WebRender::new().render_app(&app)
//!         .get("index.html")
//!         .expect("could not get rendered file").to_string()
//! }
//!
//! fn main() {
//!     run!().expect("could not start Vial server");
//! }
//! ```
//!
//! Some Javascript code, used for progressive enhancement, which automatically replaces the
//! modified content of a page after an [`InteractiveGroup`] is submitted, preventing continuously
//! reloading pages, can be embed in the page using the [`WebRender::js_progressive_enhancement`] function.
#![doc(html_playground_url = "https://play.rust-lang.org/")]
#![doc(html_logo_url = "https://gitlab.com/uillinn/artwork/-/raw/main/logo/uillinn-logo.svg")]
#![warn(missing_docs)]
mod component;
mod layout;

use component::{utils::format_id, *};

use uillinn_core::prelude::*;

/// Structure implementing the [`Render`] trait used to transform components into HTML and CSS
///
/// Some Javascript code, used for progressive enhancement, which automatically replaces the
/// modified content of a page after an [`InteractiveGroup`] is submitted, preventing continuously
/// reloading pages, can be embed in the page using the [`WebRender::js_progressive_enhancement`] function.
///
/// [`Render`]: `uillinn_core::prelude::Render`
pub struct WebRender {
    js_progressive_enhancement: bool,
}

impl WebRender {
    /// Initialize a new [`WebRender`]
    pub fn new() -> Self {
        Self {
            js_progressive_enhancement: false,
        }
    }

    /// Embed some Javascript code used for progressive enhancement, which automatically replaces
    /// the modified content of a page after a is submitted
    pub fn js_progressive_enhancement(mut self) -> Self {
        self.js_progressive_enhancement = true;
        self
    }
}

impl Render for WebRender {
    type Result = String;

    fn render_app(&mut self, app: &App) -> Self::Result {
        let mut additional_styles = String::new();
        let result = render_to_string(None, &app.root, &mut additional_styles);

        format!(
            "<!DOCTYPE HTML>\
<html lang=\"{}\">\
<head>\
<meta charset=\"utf-8\" />\
<meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />\
<title>{}</title>\
<meta name=\"description\" content=\"{}\" />\
<style>{}{}</style>\
</head>\
<body style=\"margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;\">\
{}\
{}\
</body>\
</html>",
            app.lang,
            app.name,
            app.description,
            component::DEFAULT_STYLES,
            additional_styles,
            result,
            if self.js_progressive_enhancement {
                component::DEFAULT_SCRIPT
            } else {
                ""
            },
        )
    }
}

impl Default for WebRender {
    fn default() -> Self {
        Self::new()
    }
}

pub(crate) fn calc_layout(
    area: &Area,
    component: &Component,
    maybe_computed_hash: Option<String>,
) -> (String, String, Option<String>) {
    // Compute the layout and the additional styles containing screen-based styles (if needed)
    let (layout, computed_additional_styles) = layout::css_layout(area);

    if computed_additional_styles.as_str() != "" {
        // Some screen-based styles are needed, compute the hash of the component and
        // replace it in the styles
        let hash = if let Some(hash) = maybe_computed_hash {
            hash
        } else {
            component.get_hash(area)
        };

        (
            layout,
            computed_additional_styles.replace(WILL_BE_REPLACED_BY_HASH, hash.as_str()),
            Some(hash),
        )
    } else if let ComponentKind::Interactive(InteractiveComponentKind::BooleanInput(_)) =
        component.kind
    {
        let hash = if let Some(hash) = maybe_computed_hash {
            hash
        } else {
            component.get_hash(area)
        };

        (layout, "".to_string(), Some(hash))
    } else {
        // No need to add additional styles and to compute the hash
        (layout, "".to_string(), None)
    }
}

fn render_to_string(
    area: Option<&Area>,
    component: &Component,
    additional_styles: &mut String,
) -> String {
    if let Some(area) = area {
        let (layout, computed_additional_styles, id) = calc_layout(area, component, None);

        let rendered_component = match component.kind {
            ComponentKind::Text(ref def) => text::render(def, &layout, id, &area.height),
            ComponentKind::Paragraph(ref def) => paragraph::render(def, &layout, id),
            ComponentKind::Rectangle(ref def) => rectangle::render(def, &layout, id),
            ComponentKind::Ellipse(ref def) => ellipse::render(def, &layout, id, area),
            ComponentKind::Path(ref def) => path::render(def, &layout, id),
            ComponentKind::Interactive(ref kind) => interactive::render(
                None,
                &NotRequired("".to_string()),
                kind,
                &layout,
                id,
                additional_styles,
            ),
            ComponentKind::Custom => format!(
                r#"<div{} style="{}">{}</div>"#,
                format_id(&id),
                layout,
                component
                    .children
                    .as_ref()
                    .unwrap()
                    .iter()
                    .map(|(a, c)| render_to_string(Some(a), c, additional_styles))
                    .collect::<String>(),
            ),
        };

        additional_styles.push_str(computed_additional_styles.as_str());
        rendered_component
    } else {
        // Special case for the root component
        if component.children.is_none() {
            render_to_string(
                Some(&Area::new(0, 0, Percent(100), Percent(100))),
                component,
                additional_styles,
            )
        } else {
            component
                .children
                .as_ref()
                .unwrap()
                .iter()
                .map(|(a, c)| render_to_string(Some(a), c, additional_styles))
                .collect::<String>()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_button() {
        let root = Component::new()
            .child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>", crate::component::DEFAULT_STYLES, "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_button_with_js_progressive_enhancement() {
        let root = Component::new()
            .child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().js_progressive_enhancement().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>", crate::component::DEFAULT_STYLES, "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                crate::component::DEFAULT_SCRIPT,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_in_right_order() {
        let root = Component::new()
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            )
            .child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::default().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>", crate::component::DEFAULT_STYLES, "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_screen_based() {
        let root = Component::new()
            .child(
                Area::new(
                    0,
                    0,
                    ScreenBased::new(Percent(50)).width_smaller_than(600, Percent(100)),
                    Percent(100),
                ),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>",
                crate::component::DEFAULT_STYLES,
                "@media screen and (max-width: 600px){.ui-640fb78487d7a3ff{width:100%!important;}}",
                "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div class="ui-640fb78487d7a3ff" style="left:0px;top:0px;width:50%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_screen_based_2() {
        let root = Component::new()
            .child(
                Area::new(
                    ScreenBased::new(StartPx(0)).width_wider_than(1000, StartPx(40)),
                    0,
                    ScreenBased::new(Percent(50)).height_smaller_than(600, Percent(100)),
                    Percent(100),
                ),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>",
                crate::component::DEFAULT_STYLES,
                "@media screen and (min-width: 1000px){.ui-72e472df19cb5995{left:40px!important;}}",
                "@media screen and (max-height: 600px){.ui-72e472df19cb5995{width:100%!important;}}",
                "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div class="ui-72e472df19cb5995" style="left:0px;top:0px;width:50%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_screen_based_several_breakpoints() {
        let root = Component::new()
            .child(
                Area::new(
                    ScreenBased::new(StartPx(0))
                        .width_wider_than(1000, StartPx(40))
                        .height_smaller_than(1500, EndPx(500)),
                    0,
                    ScreenBased::new(Percent(50)).height_smaller_than(600, Percent(100)),
                    Percent(100),
                ),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>",
                crate::component::DEFAULT_STYLES,
                "@media screen and (min-width: 1000px){.ui-dd4e6ca4b93afb5c{left:40px!important;}}",
                "@media screen and (max-height: 1500px){.ui-dd4e6ca4b93afb5c{right:500px!important;}}",
                "@media screen and (max-height: 600px){.ui-dd4e6ca4b93afb5c{width:100%!important;}}",
                "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div class="ui-dd4e6ca4b93afb5c" style="left:0px;top:0px;width:50%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_screen_based_default_center() {
        let root = Component::new().child(
            Area::new(
                ScreenBased::new(CenterPx(0)).width_wider_than(1000, StartPx(400)),
                0,
                100,
                Percent(100),
            ),
            Rectangle::new().color(Red),
        );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>",
                crate::component::DEFAULT_STYLES,
                "@media screen and (min-width: 1000px){.ui-b386de12c98619e1{left:400px!important;transform:translateX(0px)!important;}}",
                "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div class="ui-b386de12c98619e1" style="left:50%;top:0px;transform:translateX(-50%);width:100px;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_screen_based_duplicate_breakpoint() {
        // TODO: Handle this special case: when two breakpoints have the same size and the same
        // variant of the enum `ScreenBasedKind`?
        let root = Component::new()
            .child(
                Area::new(
                    ScreenBased::new(StartPx(0))
                        .width_wider_than(1000, StartPx(40))
                        .width_wider_than(1000, EndPx(500)),
                    0,
                    ScreenBased::new(Percent(50)).height_smaller_than(600, Percent(100)),
                    Percent(100),
                ),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>",
                crate::component::DEFAULT_STYLES,
                "@media screen and (min-width: 1000px){.ui-188d363589fe7bc1{left:40px!important;}}",
                "@media screen and (min-width: 1000px){.ui-188d363589fe7bc1{right:500px!important;}}",
                "@media screen and (max-height: 600px){.ui-188d363589fe7bc1{width:100%!important;}}",
                "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div class="ui-188d363589fe7bc1" style="left:0px;top:0px;width:50%;height:100%;background-color:rgb(255,65,54);"></div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;">Click me!</span>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_programmatic() {
        fn button(text: &str) -> Component {
            Component::new()
                .child(
                    Area::new(0, 0, Percent(100), Percent(100)),
                    Rectangle::new().color(Red),
                )
                .child(
                    Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                    Text::new(text).font_family("serif".to_string()),
                )
        }

        fn my_ellipse() -> Component {
            Component::new().child(
                Area::new(0, 0, 100, 100),
                Ellipse::new().start_angle(20).end_angle(80).color(Blue),
            )
        }

        fn my_path() -> Component {
            Component::new().child(
                Area::new(0, 0, 100, 100),
                Path::new((20, 20))
                    .x_line_by(20.)
                    .y_line_by(20.)
                    .x_line_by(-20.)
                    .y_line_by(-20.),
            )
        }

        let root = Component::new()
            .child(
                Area::new(0, 0, Percent(40), Percent(50)),
                button("Hello world!"),
            )
            .child(
                Area::new(StartPercent(60), 0, Percent(40), Percent(50)),
                button("Click me!"),
            )
            .child(
                Area::new(CenterPx(-120), StartPercent(50), 100, 100),
                my_ellipse(),
            )
            .child(
                Area::new(CenterPx(120), StartPercent(50), 100, 100),
                my_path(),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>", crate::component::DEFAULT_STYLES, "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div style="left:0px;top:0px;width:40%;height:50%;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);">"#,
                r#"</div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;font-family:serif;">Hello world!</span>"#,
                r#"</div>"#,
                r#"<div style="left:60%;top:0px;width:40%;height:50%;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(255,65,54);">"#,
                r#"</div>"#,
                r#"<span style="left:10%;top:10%;width:80%;height:80%;display:flex;align-items:center;justify-content:center;font-family:serif;">Click me!</span>"#,
                r#"</div>"#,
                r#"<div style="left:calc(50% + -120px);top:50%;transform:translateX(-50%);width:100px;height:100px;">"#,
                r#"<svg style="left:0px;top:0px;width:100px;height:100px;">"#,
                r#"<path d="M50 50 L67.101036 3.015274 A50.0001 50.0001 0 0 1 99.24048 41.317574 L50 50Z" fill-rule="evenodd" style="fill:rgb(0,116,217);stroke:rgb(0,0,0);stroke-width:0;" />"#,
                r#"</svg>"#,
                r#"</div>"#,
                r#"<div style="left:calc(50% + 120px);top:50%;transform:translateX(-50%);width:100px;height:100px;">"#,
                r#"<svg viewBox="0 0 20 20" preserveAspectRatio="none" style="left:0px;top:0px;width:100px;height:100px;">"#,
                r#"<path d="m0,0 h20 v20 h-20 v-20" vector-effect="non-scaling-stroke" style="stroke:rgb(0,0,0);stroke-width:0px;" />"#,
                r#"</svg>"#,
                r#"</div>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_interactive() {
        fn text_input() -> Component {
            Component::new().child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new()
                    .color(Transparent)
                    .rounded(5)
                    .border_size(1)
                    .border_color(Blue),
            )
        }

        fn button(text: &str) -> Component {
            Component::new()
                .child(
                    Area::new(0, 0, Percent(100), Percent(100)),
                    Rectangle::new().rounded(5).color(Blue),
                )
                .child(
                    Area::new(0, 0, Percent(100), Percent(100)),
                    Text::new(text).color(White),
                )
        }

        let root = Component::new()
            .child(Area::new(CENTER, 10, 500, 50), text_input())
            .child(
                Area::new(0, 10, Percent(100), Percent(100)),
                InteractiveGroup::new(POST, "/login")
                    .child(
                        "username",
                        Required,
                        Area::new(CENTER, 0, 500, 50),
                        TextInput::new()
                            .placeholder(Text::new("Enter username").color(Black.lighten(10))),
                    )
                    .child_trigger(
                        Area::new(CENTER, 75, 300, 50),
                        button("Enter your private vault"),
                    ),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            WebRender::new().render_app(&app),
            vec![
                r#"<!DOCTYPE HTML>"#,
                r#"<html lang="en">"#,
                r#"<head>"#,
                r#"<meta charset="utf-8" />"#,
                r#"<meta name="viewport" content="width=device-width,initial-scale=1" />"#,
                r#"<title>uillinn test</title>"#,
                r#"<meta name="description" content="An awesome app" />"#,
                "<style>", crate::component::DEFAULT_STYLES, "</style>",
                r#"</head>"#,
                r#"<body style="margin:0;padding:0;border:0;min-width:100vw;min-height:100vh;">"#,
                r#"<div style="left:50%;top:10px;transform:translateX(-50%);width:500px;height:50px;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;border-width:1px;border-color:rgb(0,116,217);border-radius:5px;">"#,
                r#"</div>"#,
                r#"</div>"#,
                r#"<form method="POST" action="/login" style="left:0px;top:10px;width:100%;height:100%;">"#,
                r#"<input name="username" type="text" placeholder="Enter username" style="left:50%;top:0px;transform:translateX(-50%);width:500px;height:50px;text-align:center;color:rgb(26,26,26);" required>"#,
                r#"<input type="submit" id="trigger-ui-e088d4c56a8023cf" style="opacity:0;left:50%;top:75px;transform:translateX(-50%);width:300px;height:50px;">"#,
                r#"<label for="trigger-ui-e088d4c56a8023cf" style="cursor:pointer;left:50%;top:75px;transform:translateX(-50%);width:300px;height:50px;">"#,
                r#"<div style="left:0px;top:0px;width:100%;height:100%;background-color:rgb(0,116,217);border-radius:5px;">"#,
                r#"</div>"#,
                r#"<span style="left:0px;top:0px;width:100%;height:100%;display:flex;align-items:center;justify-content:center;color:rgb(255,255,255);">Enter your private vault</span>"#,
                r#"</label>"#,
                r#"</form>"#,
                r#"</body>"#,
                r#"</html>"#,
            ]
            .join(""),
        );
    }
}
