pub(crate) mod ellipse;
pub(crate) mod interactive;
pub(crate) mod paragraph;
pub(crate) mod path;
pub(crate) mod rectangle;
pub(crate) mod text;
pub(crate) mod utils;

use font_kit::font::Font;
use std::{cell::RefCell, collections::HashMap};
use uillinn_core::component::FontFamilyList;
use utils::*;

// TODO: Move it in its own module?
thread_local! {
    pub static FONT_CACHE: RefCell<HashMap<(FontFamilyList, usize), Font>> = RefCell::new(HashMap::new());
}
