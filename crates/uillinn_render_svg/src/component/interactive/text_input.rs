use crate::layout::ComputedArea;
use uillinn_core::prelude::*;

pub fn render(def: &TextInput, layout: &ComputedArea, overflow: &str) -> String {
    crate::component::text::render(&def.placeholder, layout, overflow)
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_text_input() {
        let component = TextInput::new().placeholder(Text::new("Hello world!").color(Gray));

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
                "hidden",
            ),
            crate::component::text::render(
                &Text::new("Hello world!").color(Gray),
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
                "hidden"
            ),
        );
    }
}
