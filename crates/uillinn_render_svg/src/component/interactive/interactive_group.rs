use crate::layout::{compute_area, ComputedArea};
use uillinn_core::prelude::*;

pub fn render(
    def: &InteractiveGroup,
    parent_layout: &ComputedArea,
    _parent_overflow: &str,
    image_size: &(usize, usize),
) -> String {
    format!(
        r#"<svg x="{}" y="{}" width="{}" height="{}">{}{}</svg>"#,
        parent_layout.x,
        parent_layout.y,
        parent_layout.width,
        parent_layout.height,
        def.children
            .iter()
            .map(
                |(_name, _is_required, (layout, component))| match component.kind {
                    ComponentKind::Interactive(ref kind) => {
                        let overflow = if layout.scrollable { "auto" } else { "hidden" };
                        let area = compute_area(image_size, parent_layout, layout);

                        super::render(kind, &area, overflow, image_size)
                    }
                    _ => unreachable!(),
                }
            )
            .collect::<String>(),
        // `.unwrap()` won't fire an error because an `InteractiveGroup` must have a trigger component
        // when it is built
        crate::render_to_string_parent(
            image_size,
            Some(parent_layout),
            &def.trigger.as_ref().unwrap().0,
            &def.trigger.as_ref().unwrap().1
        ),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_interactive_group_with_interactive_components() {
        let text_input = TextInput::new();
        let group = InteractiveGroup::new(GET, "/new")
            .child(
                "my-input",
                Required,
                Area::new(0, CenterPercent(10), Percent(100), Percent(12)),
                text_input,
            )
            .child_trigger(Area::new(10, 10, 10, 10), Rectangle::new().color(Blue));

        assert_eq!(
            render(
                &group,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
                "hidden",
                &(800, 800),
            ),
            format!(
                r#"<svg x="0" y="0" width="100" height="100">{}<svg x="10" y="10" width="10" height="10" overflow="auto"><path d="m0 0 h10 a0 0 0 0 1 0 0 v10 a0 0 0 0 1 -0 0 h-10 a0 0 0 0 1 -0 -0 v-10 a0 0 0 0 1 0 -0z" fill="rgb(0,116,217)" stroke="rgb(0,0,0)" stroke-width="0" /></svg></svg>"#,
                crate::component::text::render(
                    &Text::new("Enter some text…"),
                    &ComputedArea {
                        x: 0.,
                        y: 54.,
                        width: 100.,
                        height: 12.
                    },
                    "hidden"
                )
            ),
        );
    }
}
