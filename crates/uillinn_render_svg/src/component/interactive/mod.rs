pub(crate) mod boolean_input;
pub(crate) mod interactive_group;
pub(crate) mod text_input;

use crate::layout::ComputedArea;
use uillinn_core::prelude::*;

pub fn render(
    kind: &InteractiveComponentKind,
    layout: &ComputedArea,
    overflow: &str,
    image_size: &(usize, usize),
) -> String {
    match kind {
        InteractiveComponentKind::TextInput(ref def) => text_input::render(def, layout, overflow),
        InteractiveComponentKind::BooleanInput(ref def) => {
            boolean_input::render(def, layout, overflow, image_size)
        }
        InteractiveComponentKind::InteractiveGroup(ref def) => {
            interactive_group::render(def, layout, overflow, image_size)
        }
    }
}
