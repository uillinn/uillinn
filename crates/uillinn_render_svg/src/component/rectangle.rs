use super::empty_if_default;
use crate::layout::ComputedArea;
use uillinn_core::prelude::*;

pub fn gen_path_data(def: &Rectangle, layout: &ComputedArea) -> String {
    let rounded = def.rounded.to_tuple();
    let mut rounded = (
        *rounded.0 as f32,
        *rounded.1 as f32,
        *rounded.2 as f32,
        *rounded.3 as f32,
    );

    // Algorithm from https://www.w3.org/TR/css-backgrounds-3/#corner-overlap
    let f = f32::min(
        f32::min(
            layout.width / (rounded.0 + rounded.1) as f32,
            layout.width / (rounded.2 + rounded.3) as f32,
        ),
        f32::min(
            layout.height / (rounded.1 + rounded.2) as f32,
            layout.height / (rounded.3 + rounded.0) as f32,
        ),
    );

    if f > 0. && f < 1. {
        rounded.0 *= f;
        rounded.1 *= f;
        rounded.2 *= f;
        rounded.3 *= f;
    }

    // SVG commands from https://www.w3.org/TR/SVG11/shapes.html#RectElement
    format!(
        r#"m{} {} h{} a{tr} {tr} 0 0 1 {tr} {tr} v{} a{br} {br} 0 0 1 -{br} {br} h{} a{bl} {bl} 0 0 1 -{bl} -{bl} v{} a{tl} {tl} 0 0 1 {tl} -{tl}z"#,
        rounded.0 + def.border_size as f32 / 2.,
        def.border_size as f32 / 2.,
        layout.width - rounded.1 - rounded.0 - def.border_size as f32,
        layout.height - rounded.2 - rounded.1 - def.border_size as f32,
        -(layout.width - rounded.3 - rounded.2 - def.border_size as f32),
        -(layout.height - rounded.3 - rounded.0 - def.border_size as f32),
        tl = rounded.0,
        tr = rounded.1,
        bl = rounded.3,
        br = rounded.2,
    )
}

pub fn render(def: &Rectangle, layout: &ComputedArea) -> String {
    // TODO: Is it possible to remove the `overflow="auto"` without breaking rotation?
    format!(
        r#"<svg x="{}" y="{}" width="{}" height="{}" overflow="auto"><path d="{}"{}{}{}{} /></svg>"#,
        layout.x,
        layout.y,
        layout.width,
        layout.height,
        gen_path_data(def, layout),
        empty_if_default(def.color, Black, r#" fill="{}""#),
        empty_if_default(def.border_color, Transparent, r#" stroke="{}""#),
        empty_if_default(def.border_size, 1, r#" stroke-width="{}""#),
        empty_if_default(
            def.rotation,
            0,
            r#" transform-origin="50% 50%" transform="rotate({})""#
        ),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_rectangle() {
        let component = Rectangle::new();

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m0 0 h100 a0 0 0 0 1 0 0 v100 a0 0 0 0 1 -0 0 h-100 a0 0 0 0 1 -0 -0 v-100 a0 0 0 0 1 0 -0z" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_color() {
        let component = Rectangle::new().color(Red);

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m0 0 h100 a0 0 0 0 1 0 0 v100 a0 0 0 0 1 -0 0 h-100 a0 0 0 0 1 -0 -0 v-100 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_border_size() {
        let component = Rectangle::new().border_size(2);

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m1 1 h98 a0 0 0 0 1 0 0 v98 a0 0 0 0 1 -0 0 h-98 a0 0 0 0 1 -0 -0 v-98 a0 0 0 0 1 0 -0z" stroke="rgb(0,0,0)" stroke-width="2" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_border_color() {
        let component = Rectangle::new().border_size(2).border_color(Red);

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m1 1 h98 a0 0 0 0 1 0 0 v98 a0 0 0 0 1 -0 0 h-98 a0 0 0 0 1 -0 -0 v-98 a0 0 0 0 1 0 -0z" stroke="rgb(255,65,54)" stroke-width="2" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rounded_all() {
        let component = Rectangle::new().rounded(20);

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m20 0 h60 a20 20 0 0 1 20 20 v60 a20 20 0 0 1 -20 20 h-60 a20 20 0 0 1 -20 -20 v-60 a20 20 0 0 1 20 -20z" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rounded_each() {
        let component = Rectangle::new().rounded(Each(10, 20, 30, 40));

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m10 0 h70 a20 20 0 0 1 20 20 v50 a30 30 0 0 1 -30 30 h-30 a40 40 0 0 1 -40 -40 v-50 a10 10 0 0 1 10 -10z" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_rectangle_with_rotation() {
        let component = Rectangle::new().rotate(90);

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" overflow="auto"><path d="m0 0 h100 a0 0 0 0 1 0 0 v100 a0 0 0 0 1 -0 0 h-100 a0 0 0 0 1 -0 -0 v-100 a0 0 0 0 1 0 -0z" stroke="rgb(0,0,0)" stroke-width="0" transform-origin="50% 50%" transform="rotate(90)" /></svg>"#,
        );
    }
}
