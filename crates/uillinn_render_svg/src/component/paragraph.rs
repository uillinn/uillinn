use super::{empty_if_default, FONT_CACHE};
use crate::{layout::ComputedArea, utils::PathOutlineBuilder};
use font_kit::family_name::FamilyName;
use font_kit::hinting::HintingOptions;
use font_kit::properties::{Properties, Weight};
use font_kit::source::SystemSource;
use uillinn_core::prelude::*;

pub fn render(def: &Paragraph, layout: &ComputedArea, overflow: &str) -> String {
    FONT_CACHE.with(|font_cache| {
        let mut font_cache = font_cache.borrow_mut();

        let font = if let Some(font) = font_cache.get(&(def.font_family.clone(), def.font_weight)) {
            font.clone()
        } else {
            // Load the font
            //
            // TODO: Support custom font file (aka web fonts)
            let families = def
                .font_family
                .to_iter()
                .map(|f| match f.as_str() {
                    "sans-serif" => FamilyName::SansSerif,
                    "sans" => FamilyName::SansSerif,
                    "serif" => FamilyName::Serif,
                    "cursive" => FamilyName::Cursive,
                    "fantasy" => FamilyName::Fantasy,
                    "monospace" => FamilyName::Monospace,
                    name => FamilyName::Title(name.to_string()),
                })
                .collect::<Vec<FamilyName>>();

            let mut properties = Properties::new();
            properties.weight = Weight(def.font_weight as f32);

            let font = SystemSource::new()
                .select_best_match(families.as_slice(), &properties)
                .unwrap()
                .load()
                .unwrap();

            font_cache.insert((def.font_family.clone(), def.font_weight), font.clone());

            font
        };

        // Find the path
        let mut builder = PathOutlineBuilder::new(0., 0., def.font_size as f32, font.metrics());
        let line_height = def.font_size as f32 * def.line_height as f32 / 100.;

        for word in def.content.split_inclusive(' ') {
            if def.break_word {
                for ch in word.chars() {
                    if let Some(glyph_id) = font.glyph_for_char(ch) {
                        let advance = font.advance(glyph_id).unwrap();

                        if builder.path_width() + advance.x() * builder.scale > layout.width {
                            builder.new_line(line_height);
                        }

                        font.outline(glyph_id, HintingOptions::None, &mut builder)
                            .unwrap();
                        builder.advance_by(advance.x());
                    } else if ch == '\n' {
                        builder.new_line(line_height);
                    }
                }
            } else {
                let mut word_size = 0.;

                for ch in word.chars() {
                    // Silently ignore unknown characters
                    if let Some(glyph_id) = font.glyph_for_char(ch) {
                        // TODO: Support more advanced characters
                        word_size += font.advance(glyph_id).unwrap().x() * builder.scale;
                    }
                }

                if builder.path_width() + word_size > layout.width {
                    // TODO: Support breaking with the `-` character
                    builder.new_line(line_height);
                }

                for ch in word.chars() {
                    if let Some(glyph_id) = font.glyph_for_char(ch) {
                        let advance = font.advance(glyph_id).unwrap();

                        font.outline(glyph_id, HintingOptions::None, &mut builder)
                            .unwrap();
                        builder.advance_by(advance.x());
                    } else if ch == '\n' {
                        builder.new_line(line_height);
                    }
                }
            }
        }

        format!(
            r#"<svg x="{}" y="{}" width="{}" height="{}" overflow="{}"><path d="{}"{}{} /></svg>"#,
            layout.x,
            layout.y,
            layout.width,
            layout.height,
            overflow,
            builder.get_path_data(),
            empty_if_default(def.color, Black, r#" fill="{}""#),
            empty_if_default(
                def.rotation,
                0,
                r#" transform-origin="50% 50%" transform="rotate({})""#
            ),
        )
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn render_default_paragraph() {
        let component =
            Paragraph::new(r#"Lorem ipsum dolor sit amet, consectetur adipiscing elit."#);

        render(
            &component,
            &ComputedArea {
                x: 0.,
                y: 0.,
                width: 200.,
                height: 500.,
            },
            "hidden",
        );
    }

    #[test]
    fn render_paragraph_with_break_word() {
        let component =
            Paragraph::new(r#"Lorem ipsum dolor sit amet, consectetur adipiscing elit."#)
                .break_word();

        render(
            &component,
            &ComputedArea {
                x: 0.,
                y: 0.,
                width: 200.,
                height: 500.,
            },
            "hidden",
        );
    }
}
