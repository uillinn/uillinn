use std::fmt;

pub fn empty_if_default<T: PartialEq + fmt::Display>(
    val: T,
    default: T,
    formatting: &'static str,
) -> String {
    if val == default {
        String::new()
    } else {
        formatting.replace("{}", val.to_string().as_str())
    }
}
