use uillinn_core::{layout::ScreenBasedKind, prelude::*};

#[derive(Debug, Clone, PartialEq)]
pub struct ComputedArea {
    pub x: f32,
    pub y: f32,
    pub width: f32,
    pub height: f32,
}

impl ComputedArea {
    pub fn from_pixels(x: f32, y: f32, width: f32, height: f32) -> Self {
        Self {
            x,
            y,
            width,
            height,
        }
    }
}

pub fn adapt_screen_to_image<'a, 'b, T>(
    image_size: &'a (usize, usize),
    val: &'b ScreenBased<T>,
) -> &'b T {
    val.breakpoints
        .iter()
        .find_map(|b| match b {
            ScreenBasedKind::WidthSmallerThan(breakpoint, inner_val) => {
                if image_size.0 < *breakpoint {
                    Some(inner_val)
                } else {
                    None
                }
            }
            ScreenBasedKind::WidthWiderThan(breakpoint, inner_val) => {
                if image_size.0 > *breakpoint {
                    Some(inner_val)
                } else {
                    None
                }
            }
            ScreenBasedKind::HeightSmallerThan(breakpoint, inner_val) => {
                if image_size.1 < *breakpoint {
                    Some(inner_val)
                } else {
                    None
                }
            }
            ScreenBasedKind::HeightWiderThan(breakpoint, inner_val) => {
                if image_size.1 > *breakpoint {
                    Some(inner_val)
                } else {
                    None
                }
            }
        })
        .unwrap_or(&val.default)
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn adapt_screen_to_image_test() {
        assert_eq!(
            adapt_screen_to_image(&(800, 500), &ScreenBased::<Size>::new(Size::Px(10))),
            &Size::Px(10)
        );
        assert_eq!(
            adapt_screen_to_image(
                &(800, 500),
                &ScreenBased::<Size>::new(Size::Px(10)).width_smaller_than(900, Size::Px(100))
            ),
            &Size::Px(100)
        );
        assert_eq!(
            adapt_screen_to_image(
                &(800, 500),
                &ScreenBased::<Size>::new(Size::Px(10)).width_wider_than(200, Size::Px(50))
            ),
            &Size::Px(50)
        );
        assert_eq!(
            adapt_screen_to_image(
                &(800, 500),
                &ScreenBased::<Size>::new(Size::Px(10)).height_smaller_than(600, Size::Px(20))
            ),
            &Size::Px(20)
        );
        assert_eq!(
            adapt_screen_to_image(
                &(800, 500),
                &ScreenBased::<Size>::new(Size::Px(10)).height_wider_than(300, Size::Px(30))
            ),
            &Size::Px(30)
        );
    }
}
