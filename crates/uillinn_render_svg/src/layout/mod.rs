use uillinn_core::prelude::*;

mod utils;

pub use utils::{adapt_screen_to_image, ComputedArea};

pub fn compute_area(
    image_size: &(usize, usize),
    parent_area: &ComputedArea,
    area: &Area,
) -> ComputedArea {
    fn compute_size(parent: &f32, val: &Size) -> f32 {
        match val {
            Size::Px(val) => *val as f32,
            Size::Percent(val) => parent * *val as f32 / 100.,
        }
    }

    fn compute_pos(parent_size: &f32, size: f32, val: &Pos) -> f32 {
        match val {
            Pos::StartPx(val) => *val as f32,
            Pos::StartPercent(val) => parent_size * *val as f32 / 100.,
            Pos::CenterPx(val) => (parent_size - size) / 2. + *val as f32,
            Pos::CenterPercent(val) => {
                (parent_size - size) / 2. + (parent_size * *val as f32 / 100.)
            }
            Pos::EndPx(val) => parent_size - size - *val as f32,
            Pos::EndPercent(val) => parent_size - size - parent_size * *val as f32 / 100.,
        }
    }

    let width = compute_size(
        &parent_area.width,
        adapt_screen_to_image(image_size, &area.width),
    );
    let height = compute_size(
        &parent_area.height,
        adapt_screen_to_image(image_size, &area.height),
    );
    let x = compute_pos(
        &parent_area.width,
        width,
        adapt_screen_to_image(image_size, &area.x_axis),
    );
    let y = compute_pos(
        &parent_area.height,
        height,
        adapt_screen_to_image(image_size, &area.y_axis),
    );

    ComputedArea {
        x,
        y,
        width,
        height,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn compute_area_simple() {
        assert_eq!(
            compute_area(
                &(500, 700),
                &ComputedArea::from_pixels(100., 80., 200., 150.),
                &Area::new(10, 10, 80, 80)
            ),
            ComputedArea {
                x: 10.,
                y: 10.,
                width: 80.,
                height: 80.,
            },
        );
    }

    #[test]
    fn compute_area_percent() {
        assert_eq!(
            compute_area(
                &(500, 700),
                &ComputedArea::from_pixels(100., 80., 200., 150.),
                &Area::new(10, StartPercent(10), Percent(80), 80)
            ),
            ComputedArea {
                x: 10.,
                y: 15.,
                width: 160.,
                height: 80.,
            },
        );
    }

    #[test]
    fn compute_area_screen_based() {
        assert_eq!(
            compute_area(
                &(500, 700),
                &ComputedArea::from_pixels(100., 80., 200., 150.),
                &Area::new(
                    ScreenBased::new(5).width_smaller_than(600, 55),
                    StartPercent(10),
                    Percent(80),
                    80
                )
            ),
            ComputedArea {
                x: 55.,
                y: 15.,
                width: 160.,
                height: 80.,
            },
        );
    }
}
