//! Render components to an SVG file
//!
//! NOTE: This render cannot be used to build neither interactive nor responsive images. If you
//! want to build a website, use [`uillinn_render_web`](https://uillinn.gitlab.io/uillinn/uillinn_render_web)
//! instead.
//!
//! NOTE: This render is converting all texts to SVG paths using the fonts installed on your system, so you
//! can easily send it to your co-workers and they will see the texts with the fonts you have.
//!
//! # Not supported
//!
//! - `Alignment::Left`, `Alignment::Center`, `Alignment::Right` and `Alignment::Justify` will be ignored in `Paragraph` components
//!
//! # Example for generating an SVG file in a directory
//!
//! ```rust
//! use uillinn_core::prelude::*;
//! use uillinn_render_svg::SvgRender;
//!
//! fn main() {
//!     // Start by creating a root component with a red text at (0, 0) (by default the text is centered)
//!     let root = Component::new().child(
//!         Area::new(0, 0, Percent(100), 50),
//!         Text::new("Hello world!").color(Red),
//!     );
//!
//!     // Then, define an `App` containing some metadata about the application (name, description and language of it)
//!     let app = App::new("Hello world!", "Just want to say hello", "en", root);
//!
//!     // Finally, display the code of the SVG image (of size 1280×800) generated
//!     println!("{}", SvgRender::new(1280, 800).render_app(&app));
//! }
//! ```
#![doc(html_playground_url = "https://play.rust-lang.org/")]
#![doc(html_logo_url = "https://gitlab.com/uillinn/artwork/-/raw/main/logo/uillinn-logo.svg")]
#![warn(missing_docs)]
mod component;
mod layout;
mod utils;

use component::*;
use layout::{compute_area, ComputedArea};

use uillinn_core::prelude::*;

/// Structure implementing the [`Render`] trait used to transform components SVG
///
/// [`Render`]: `uillinn_core::prelude::Render`
pub struct SvgRender {
    image_width: usize,
    image_height: usize,
}

impl SvgRender {
    /// Create a new [`SvgRender`]
    pub fn new(image_width: usize, image_height: usize) -> Self {
        Self {
            image_width,
            image_height,
        }
    }
}

impl Render for SvgRender {
    type Result = String;

    fn render_app(&mut self, app: &App) -> Self::Result {
        let image_area = Area::new(0, 0, self.image_width, self.image_height);
        format!(
            r#"<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" width="{}" height="{}">{}</svg>"#,
            self.image_width,
            self.image_height,
            render_to_string_parent(
                &(self.image_width, self.image_height),
                None,
                &image_area,
                &app.root
            )
        )
    }
}

fn render_to_string_parent(
    image_size: &(usize, usize),
    parent_area: Option<&ComputedArea>,
    area: &Area,
    component: &Component,
) -> String {
    if let Some(parent_area) = parent_area {
        // Compute the layout and the additional styles containing screen-based styles (if needed)
        let layout = compute_area(image_size, parent_area, area);
        let overflow = if area.scrollable { "auto" } else { "hidden" };

        let rendered_component = match component.kind {
            ComponentKind::Text(ref def) => text::render(def, &layout, overflow),
            ComponentKind::Paragraph(ref def) => paragraph::render(def, &layout, overflow),
            ComponentKind::Rectangle(ref def) => rectangle::render(def, &layout),
            ComponentKind::Ellipse(ref def) => ellipse::render(def, &layout, overflow),
            ComponentKind::Path(ref def) => path::render(def, &layout),
            ComponentKind::Interactive(ref def) => {
                interactive::render(def, &layout, overflow, image_size)
            }
            ComponentKind::Custom => format!(
                r#"<svg x="{}" y="{}" width="{}" height="{}">{}</svg>"#,
                layout.x,
                layout.y,
                layout.width,
                layout.height,
                component
                    .children
                    .as_ref()
                    .unwrap()
                    .iter()
                    .map(|(a, c)| render_to_string_parent(image_size, Some(&layout), a, c))
                    .collect::<String>(),
            ),
        };

        rendered_component
    } else {
        // Special case for the root component
        return component
            .children
            .as_ref()
            .unwrap()
            .iter()
            .map(|(a, c)| {
                render_to_string_parent(
                    image_size,
                    Some(&ComputedArea::from_pixels(
                        0.,
                        0.,
                        image_size.0 as f32,
                        image_size.1 as f32,
                    )),
                    a,
                    c,
                )
            })
            .collect::<String>();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_button() {
        let root = Component::new()
            .child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            SvgRender::new(1280, 800).render_app(&app),
            vec![
                r#"<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">"#,
                r#"<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" width="1280" height="800">"#,
                r#"<svg x="0" y="0" width="1280" height="800" overflow="auto">"#,
                r#"<path d="m0 0 h1280 a0 0 0 0 1 0 0 v800 a0 0 0 0 1 -0 0 h-1280 a0 0 0 0 1 -0 -0 v-800 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"<svg x="128" y="80" width="1024" height="640" overflow="hidden">"#,
                r#"<path d="M484.77734 314.77344L484.77734 316.4375Q483.98047 315.6953 483.07813 315.32813Q482.17578 314.96094 481.16016 314.96094Q479.16016 314.96094 478.09766 316.1836Q477.03516 317.40625 477.03516 319.71875Q477.03516 322.02344 478.09766 323.2461Q479.16016 324.46875 481.16016 324.46875Q482.17578 324.46875 483.07813 324.10156Q483.98047 323.73438 484.77734 322.9922L484.77734 324.64063Q483.94922 325.20313 483.02344 325.48438Q482.09766 325.76563 481.0664 325.76563Q478.41797 325.76563 476.89453 324.14453Q475.3711 322.52344 475.3711 319.71875Q475.3711 316.90625 476.89453 315.28516Q478.41797 313.66406 481.0664 313.66406Q482.11328 313.66406 483.03906 313.9414Q483.96484 314.21875 484.77734 314.77344ZM487.15234 313.3828L488.58984 313.3828L488.58984 325.53906L487.15234 325.53906ZM491.59766 316.78906L493.03516 316.78906L493.03516 325.53906L491.59766 325.53906ZM491.59766 313.3828L493.03516 313.3828L493.03516 315.20313L491.59766 315.20313ZM502.33984 317.125L502.33984 318.46875Q501.73047 318.1328 501.1172 317.96484Q500.5039 317.79688 499.8789 317.79688Q498.48047 317.79688 497.70703 318.6836Q496.9336 319.5703 496.9336 321.17188Q496.9336 322.77344 497.70703 323.66016Q498.48047 324.54688 499.8789 324.54688Q500.5039 324.54688 501.1172 324.3789Q501.73047 324.21094 502.33984 323.875L502.33984 325.20313Q501.73828 325.48438 501.09375 325.625Q500.44922 325.76563 499.72266 325.76563Q497.7461 325.76563 496.58203 324.52344Q495.41797 323.28125 495.41797 321.17188Q495.41797 319.03125 496.59375 317.8047Q497.76953 316.57813 499.8164 316.57813Q500.48047 316.57813 501.11328 316.71484Q501.7461 316.85156 502.33984 317.125ZM504.78516 313.3828L506.23047 313.3828L506.23047 320.5625L510.51953 316.78906L512.35547 316.78906L507.71484 320.8828L512.5508 325.53906L510.67578 325.53906L506.23047 321.26563L506.23047 325.53906L504.78516 325.53906ZM526.0039 318.46875Q526.54297 317.5 527.29297 317.03906Q528.04297 316.57813 529.0586 316.57813Q530.4258 316.57813 531.16797 317.53516Q531.91016 318.4922 531.91016 320.2578L531.91016 325.53906L530.46484 325.53906L530.46484 320.3047Q530.46484 319.04688 530.01953 318.4375Q529.5742 317.82813 528.66016 317.82813Q527.54297 317.82813 526.89453 318.5703Q526.2461 319.3125 526.2461 320.59375L526.2461 325.53906L524.8008 325.53906L524.8008 320.3047Q524.8008 319.03906 524.35547 318.4336Q523.91016 317.82813 522.98047 317.82813Q521.8789 317.82813 521.23047 318.57422Q520.58203 319.3203 520.58203 320.59375L520.58203 325.53906L519.1367 325.53906L519.1367 316.78906L520.58203 316.78906L520.58203 318.14844Q521.0742 317.34375 521.7617 316.96094Q522.4492 316.57813 523.39453 316.57813Q524.34766 316.57813 525.0156 317.0625Q525.6836 317.54688 526.0039 318.46875ZM542.2617 320.8047L542.2617 321.5078L535.65234 321.5078Q535.7461 322.9922 536.5469 323.76953Q537.34766 324.54688 538.77734 324.54688Q539.60547 324.54688 540.3828 324.34375Q541.16016 324.14063 541.9258 323.73438L541.9258 325.09375Q541.15234 325.42188 540.33984 325.59375Q539.52734 325.76563 538.6914 325.76563Q536.59766 325.76563 535.375 324.54688Q534.15234 323.32813 534.15234 321.25Q534.15234 319.10156 535.3125 317.83984Q536.47266 316.57813 538.4414 316.57813Q540.20703 316.57813 541.2344 317.71484Q542.2617 318.85156 542.2617 320.8047ZM540.8242 320.3828Q540.8086 319.20313 540.16406 318.5Q539.51953 317.79688 538.45703 317.79688Q537.2539 317.79688 536.53125 318.47656Q535.8086 319.15625 535.6992 320.39063ZM545.52734 323.5547L547.1133 323.5547L547.1133 325.53906L545.52734 325.53906ZM545.52734 313.875L547.1133 313.875L547.1133 318.9922L546.95703 321.78125L545.6914 321.78125L545.52734 318.9922Z" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_in_right_order() {
        let root = Component::new()
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            )
            .child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            SvgRender::new(1280, 800).render_app(&app),
            vec![
                r#"<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">"#,
                r#"<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" width="1280" height="800">"#,
                r#"<svg x="128" y="80" width="1024" height="640" overflow="hidden">"#,
                r#"<path d="M484.77734 314.77344L484.77734 316.4375Q483.98047 315.6953 483.07813 315.32813Q482.17578 314.96094 481.16016 314.96094Q479.16016 314.96094 478.09766 316.1836Q477.03516 317.40625 477.03516 319.71875Q477.03516 322.02344 478.09766 323.2461Q479.16016 324.46875 481.16016 324.46875Q482.17578 324.46875 483.07813 324.10156Q483.98047 323.73438 484.77734 322.9922L484.77734 324.64063Q483.94922 325.20313 483.02344 325.48438Q482.09766 325.76563 481.0664 325.76563Q478.41797 325.76563 476.89453 324.14453Q475.3711 322.52344 475.3711 319.71875Q475.3711 316.90625 476.89453 315.28516Q478.41797 313.66406 481.0664 313.66406Q482.11328 313.66406 483.03906 313.9414Q483.96484 314.21875 484.77734 314.77344ZM487.15234 313.3828L488.58984 313.3828L488.58984 325.53906L487.15234 325.53906ZM491.59766 316.78906L493.03516 316.78906L493.03516 325.53906L491.59766 325.53906ZM491.59766 313.3828L493.03516 313.3828L493.03516 315.20313L491.59766 315.20313ZM502.33984 317.125L502.33984 318.46875Q501.73047 318.1328 501.1172 317.96484Q500.5039 317.79688 499.8789 317.79688Q498.48047 317.79688 497.70703 318.6836Q496.9336 319.5703 496.9336 321.17188Q496.9336 322.77344 497.70703 323.66016Q498.48047 324.54688 499.8789 324.54688Q500.5039 324.54688 501.1172 324.3789Q501.73047 324.21094 502.33984 323.875L502.33984 325.20313Q501.73828 325.48438 501.09375 325.625Q500.44922 325.76563 499.72266 325.76563Q497.7461 325.76563 496.58203 324.52344Q495.41797 323.28125 495.41797 321.17188Q495.41797 319.03125 496.59375 317.8047Q497.76953 316.57813 499.8164 316.57813Q500.48047 316.57813 501.11328 316.71484Q501.7461 316.85156 502.33984 317.125ZM504.78516 313.3828L506.23047 313.3828L506.23047 320.5625L510.51953 316.78906L512.35547 316.78906L507.71484 320.8828L512.5508 325.53906L510.67578 325.53906L506.23047 321.26563L506.23047 325.53906L504.78516 325.53906ZM526.0039 318.46875Q526.54297 317.5 527.29297 317.03906Q528.04297 316.57813 529.0586 316.57813Q530.4258 316.57813 531.16797 317.53516Q531.91016 318.4922 531.91016 320.2578L531.91016 325.53906L530.46484 325.53906L530.46484 320.3047Q530.46484 319.04688 530.01953 318.4375Q529.5742 317.82813 528.66016 317.82813Q527.54297 317.82813 526.89453 318.5703Q526.2461 319.3125 526.2461 320.59375L526.2461 325.53906L524.8008 325.53906L524.8008 320.3047Q524.8008 319.03906 524.35547 318.4336Q523.91016 317.82813 522.98047 317.82813Q521.8789 317.82813 521.23047 318.57422Q520.58203 319.3203 520.58203 320.59375L520.58203 325.53906L519.1367 325.53906L519.1367 316.78906L520.58203 316.78906L520.58203 318.14844Q521.0742 317.34375 521.7617 316.96094Q522.4492 316.57813 523.39453 316.57813Q524.34766 316.57813 525.0156 317.0625Q525.6836 317.54688 526.0039 318.46875ZM542.2617 320.8047L542.2617 321.5078L535.65234 321.5078Q535.7461 322.9922 536.5469 323.76953Q537.34766 324.54688 538.77734 324.54688Q539.60547 324.54688 540.3828 324.34375Q541.16016 324.14063 541.9258 323.73438L541.9258 325.09375Q541.15234 325.42188 540.33984 325.59375Q539.52734 325.76563 538.6914 325.76563Q536.59766 325.76563 535.375 324.54688Q534.15234 323.32813 534.15234 321.25Q534.15234 319.10156 535.3125 317.83984Q536.47266 316.57813 538.4414 316.57813Q540.20703 316.57813 541.2344 317.71484Q542.2617 318.85156 542.2617 320.8047ZM540.8242 320.3828Q540.8086 319.20313 540.16406 318.5Q539.51953 317.79688 538.45703 317.79688Q537.2539 317.79688 536.53125 318.47656Q535.8086 319.15625 535.6992 320.39063ZM545.52734 323.5547L547.1133 323.5547L547.1133 325.53906L545.52734 325.53906ZM545.52734 313.875L547.1133 313.875L547.1133 318.9922L546.95703 321.78125L545.6914 321.78125L545.52734 318.9922Z" />"#,
                r#"</svg>"#,
                r#"<svg x="0" y="0" width="1280" height="800" overflow="auto">"#,
                r#"<path d="m0 0 h1280 a0 0 0 0 1 0 0 v800 a0 0 0 0 1 -0 0 h-1280 a0 0 0 0 1 -0 -0 v-800 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_custom_part_of_screen() {
        let root = Component::new()
            .child(
                Area::new(
                    0,
                    0,
                    ScreenBased::new(Percent(50)).width_smaller_than(600, Percent(100)),
                    Percent(100),
                ),
                Rectangle::new().color(Red),
            )
            .child(
                Area::new(StartPercent(10), StartPercent(10), Percent(80), Percent(80)),
                Text::new("Click me!"),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            SvgRender::new(800, 800).render_app(&app),
            vec![
                r#"<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">"#,
                r#"<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" width="800" height="800">"#,
                r#"<svg x="0" y="0" width="400" height="800" overflow="auto">"#,
                r#"<path d="m0 0 h400 a0 0 0 0 1 0 0 v800 a0 0 0 0 1 -0 0 h-400 a0 0 0 0 1 -0 -0 v-800 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"<svg x="80" y="80" width="640" height="640" overflow="hidden">"#,
                r#"<path d="M292.77734 314.77344L292.77734 316.4375Q291.98047 315.6953 291.07813 315.32813Q290.17578 314.96094 289.16016 314.96094Q287.16016 314.96094 286.09766 316.1836Q285.03516 317.40625 285.03516 319.71875Q285.03516 322.02344 286.09766 323.2461Q287.16016 324.46875 289.16016 324.46875Q290.17578 324.46875 291.07813 324.10156Q291.98047 323.73438 292.77734 322.9922L292.77734 324.64063Q291.94922 325.20313 291.02344 325.48438Q290.09766 325.76563 289.0664 325.76563Q286.41797 325.76563 284.89453 324.14453Q283.3711 322.52344 283.3711 319.71875Q283.3711 316.90625 284.89453 315.28516Q286.41797 313.66406 289.0664 313.66406Q290.11328 313.66406 291.03906 313.9414Q291.96484 314.21875 292.77734 314.77344ZM295.15234 313.3828L296.58984 313.3828L296.58984 325.53906L295.15234 325.53906ZM299.59766 316.78906L301.03516 316.78906L301.03516 325.53906L299.59766 325.53906ZM299.59766 313.3828L301.03516 313.3828L301.03516 315.20313L299.59766 315.20313ZM310.33984 317.125L310.33984 318.46875Q309.73047 318.1328 309.1172 317.96484Q308.5039 317.79688 307.8789 317.79688Q306.48047 317.79688 305.70703 318.6836Q304.9336 319.5703 304.9336 321.17188Q304.9336 322.77344 305.70703 323.66016Q306.48047 324.54688 307.8789 324.54688Q308.5039 324.54688 309.1172 324.3789Q309.73047 324.21094 310.33984 323.875L310.33984 325.20313Q309.73828 325.48438 309.09375 325.625Q308.44922 325.76563 307.72266 325.76563Q305.7461 325.76563 304.58203 324.52344Q303.41797 323.28125 303.41797 321.17188Q303.41797 319.03125 304.59375 317.8047Q305.76953 316.57813 307.8164 316.57813Q308.48047 316.57813 309.11328 316.71484Q309.7461 316.85156 310.33984 317.125ZM312.78516 313.3828L314.23047 313.3828L314.23047 320.5625L318.51953 316.78906L320.35547 316.78906L315.71484 320.8828L320.55078 325.53906L318.67578 325.53906L314.23047 321.26563L314.23047 325.53906L312.78516 325.53906ZM334.0039 318.46875Q334.54297 317.5 335.29297 317.03906Q336.04297 316.57813 337.0586 316.57813Q338.42578 316.57813 339.16797 317.53516Q339.91016 318.4922 339.91016 320.2578L339.91016 325.53906L338.46484 325.53906L338.46484 320.3047Q338.46484 319.04688 338.01953 318.4375Q337.57422 317.82813 336.66016 317.82813Q335.54297 317.82813 334.89453 318.5703Q334.2461 319.3125 334.2461 320.59375L334.2461 325.53906L332.80078 325.53906L332.80078 320.3047Q332.80078 319.03906 332.35547 318.4336Q331.91016 317.82813 330.98047 317.82813Q329.8789 317.82813 329.23047 318.57422Q328.58203 319.3203 328.58203 320.59375L328.58203 325.53906L327.13672 325.53906L327.13672 316.78906L328.58203 316.78906L328.58203 318.14844Q329.07422 317.34375 329.76172 316.96094Q330.44922 316.57813 331.39453 316.57813Q332.34766 316.57813 333.01563 317.0625Q333.6836 317.54688 334.0039 318.46875ZM350.26172 320.8047L350.26172 321.5078L343.65234 321.5078Q343.7461 322.9922 344.54688 323.76953Q345.34766 324.54688 346.77734 324.54688Q347.60547 324.54688 348.3828 324.34375Q349.16016 324.14063 349.92578 323.73438L349.92578 325.09375Q349.15234 325.42188 348.33984 325.59375Q347.52734 325.76563 346.6914 325.76563Q344.59766 325.76563 343.375 324.54688Q342.15234 323.32813 342.15234 321.25Q342.15234 319.10156 343.3125 317.83984Q344.47266 316.57813 346.4414 316.57813Q348.20703 316.57813 349.23438 317.71484Q350.26172 318.85156 350.26172 320.8047ZM348.82422 320.3828Q348.8086 319.20313 348.16406 318.5Q347.51953 317.79688 346.45703 317.79688Q345.2539 317.79688 344.53125 318.47656Q343.8086 319.15625 343.69922 320.39063ZM353.52734 323.5547L355.11328 323.5547L355.11328 325.53906L353.52734 325.53906ZM353.52734 313.875L355.11328 313.875L355.11328 318.9922L354.95703 321.78125L353.6914 321.78125L353.52734 318.9922Z" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
            ]
            .join(""),
        );
    }

    #[test]
    fn render_programmatic() {
        fn button() -> Component {
            Component::new().child(
                Area::new(0, 0, Percent(100), Percent(100)),
                Rectangle::new().color(Red),
            )
        }

        fn my_ellipse() -> Component {
            Component::new().child(
                Area::new(0, 0, 100, 100),
                Ellipse::new().start_angle(20).end_angle(80).color(Blue),
            )
        }

        fn my_path() -> Component {
            Component::new().child(
                Area::new(0, 0, 100, 100),
                Path::new((20, 20))
                    .x_line_by(20.)
                    .y_line_by(20.)
                    .x_line_by(-20.)
                    .y_line_by(-20.),
            )
        }

        let root = Component::new()
            .child(Area::new(0, 0, Percent(40), Percent(50)), button())
            .child(
                Area::new(StartPercent(60), 0, Percent(40), Percent(50)),
                button(),
            )
            .child(
                Area::new(CenterPx(-120), StartPercent(50), 100, 100),
                my_ellipse(),
            )
            .child(
                Area::new(CenterPx(120), StartPercent(50), 100, 100),
                my_path(),
            );

        let app = App::new("uillinn test", "An awesome app", "en", root);

        assert_eq!(
            SvgRender::new(1280, 800).render_app(&app),
            vec![
                r#"<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">"#,
                r#"<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" width="1280" height="800">"#,
                r#"<svg x="0" y="0" width="512" height="400"><svg x="0" y="0" width="512" height="400" overflow="auto">"#,
                r#"<path d="m0 0 h512 a0 0 0 0 1 0 0 v400 a0 0 0 0 1 -0 0 h-512 a0 0 0 0 1 -0 -0 v-400 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
                r#"<svg x="768" y="0" width="512" height="400">"#,
                r#"<svg x="0" y="0" width="512" height="400" overflow="auto">"#,
                r#"<path d="m0 0 h512 a0 0 0 0 1 0 0 v400 a0 0 0 0 1 -0 0 h-512 a0 0 0 0 1 -0 -0 v-400 a0 0 0 0 1 0 -0z" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
                r#"<svg x="470" y="400" width="100" height="100">"#,
                r#"<svg x="0" y="0" width="100" height="100" overflow="hidden">"#,
                r#"<path d="M50 50 L67.101036 3.015274 A50.0001 50.0001 0 0 1 99.24048 41.317574 L50 50Z" fill-rule="evenodd" fill="rgb(0,116,217)" stroke="rgb(0,0,0)" stroke-width="0">"#,
                r#"</path>"#,
                r#"</svg>"#,
                r#"</svg>"#,
                r#"<svg x="710" y="400" width="100" height="100">"#,
                r#"<svg x="0" y="0" width="100" height="100" viewBox="0 0 20 20" preserveAspectRatio="none">"#,
                r#"<path d="m0,0 h20 v20 h-20 v-20" vector-effect="non-scaling-stroke" stroke="rgb(0,0,0)" stroke-width="0" />"#,
                r#"</svg>"#,
                r#"</svg>"#,
                r#"</svg>"#,
            ]
            .join(""),
        );
    }
}
