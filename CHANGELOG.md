# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## [0.8.0](https://gitlab.com/uillinn/uillinn/compare/0.7.1..0.8.0) - 2022-04-06
#### Bug Fixes
- **(clippy)** fix `clippy` warnings - ([43c0ba7](https://gitlab.com/uillinn/uillinn/commit/43c0ba7310e18dc7b0b02f3f3fbd73e5f5455bf7)) - Nifou
- **(uillinn_core)** fix `Paragraph`s default font - ([ae8f4ec](https://gitlab.com/uillinn/uillinn/commit/ae8f4eca162b355c182755fdf8ab90b960d62043)) - Nifou
- **(uillinn_render_web)** fix javascript progressive enhancement code - ([7ee057d](https://gitlab.com/uillinn/uillinn/commit/7ee057d2d6adf6dd1baae7cad65714c19cced252)) - Nifou
- **(uillinn_render_web)** fix text bad alignment - ([c732127](https://gitlab.com/uillinn/uillinn/commit/c732127364805cdadc53b4bcd44387b05bf36272)) - Nifou
- **(uillinn_render_web)** improve keyboard navigation in `InteractiveGroup`s - ([99f7428](https://gitlab.com/uillinn/uillinn/commit/99f7428e9e8b4c7422f449d02f76047d94958748)) - Nifou
- don't replace newlines in generated applications (+ run `cargo fmt`) - ([38b3a33](https://gitlab.com/uillinn/uillinn/commit/38b3a336c6345d0f5ec4e68a59d7c74d13d11759)) - Nifou
#### Build system
- bump `font-kit` to `0.11.0` - ([d054586](https://gitlab.com/uillinn/uillinn/commit/d054586deaa77214d798a785243060397e98d3e6)) - Nifou
#### Continuous Integration
- update `grcov` - ([24344d1](https://gitlab.com/uillinn/uillinn/commit/24344d13a0c6481c2296a1688af8adc64b5f76a8)) - Nifou
#### Documentation
- **(readme)** add `deps.rs` badge - ([461910b](https://gitlab.com/uillinn/uillinn/commit/461910b8728ff3015d8f0affe4f56cc3280260ba)) - Nifou
- **(readme)** add more badges and use official instance of `tokei` - ([154ef17](https://gitlab.com/uillinn/uillinn/commit/154ef17ef6af059f889d6cebc68f5b2a33b43969)) - Nifou
- **(readme)** add `Total lines` badge - ([b96138e](https://gitlab.com/uillinn/uillinn/commit/b96138e99fd36c9afb75f97c080bfc02e03008f1)) - Nifou
- **(readme)** fix code example - ([9db2b7a](https://gitlab.com/uillinn/uillinn/commit/9db2b7a385e79fa61d29fd4164e05eb7db8d0734)) - Nifou
#### Features
- **(uillinn_render_svg)** render interactive components - ([38b3429](https://gitlab.com/uillinn/uillinn/commit/38b3429c0c73be45b79e5609839fd2cd0032cabf)) - Nifou
- **(uillinn_render_web)** enable javascript progressive enhancement (disabled by default) - ([3bb13b1](https://gitlab.com/uillinn/uillinn/commit/3bb13b1bbdb723c83204f0a40799cbd25ee2cd40)) - Nifou
- add a `BooleanInput` interactive component - ([700548e](https://gitlab.com/uillinn/uillinn/commit/700548eecc6fcd49a8efb98ab2e214e327b29c37)) - Nifou
- use a `Text` to represent placeholders of `TextInput`s - ([e8eace4](https://gitlab.com/uillinn/uillinn/commit/e8eace42748a80b59251c59aab9e2ba129052689)) - Nifou
- support the `\n` characters in `Paragraph`s - ([4b59f8b](https://gitlab.com/uillinn/uillinn/commit/4b59f8b024286ce215962f4cea41896b72654c69)) - Nifou
- support executing an action when a trigger component is pressed in an `InteractiveGroup` - ([27d6369](https://gitlab.com/uillinn/uillinn/commit/27d63695c7679fcdb1f551e5ade93bc10d931515)) - Nifou
- add an `InteractiveGroup` interactive component - ([db60d4b](https://gitlab.com/uillinn/uillinn/commit/db60d4b835a62ed6884409fab4b2a7d40b78b186)) - Nifou
- add a `TextInput` interactive component - ([37745c8](https://gitlab.com/uillinn/uillinn/commit/37745c8743a935236648c54f798c5c13206e04cb)) - Nifou
#### Miscellaneous Chores
- **(deps)** bump `pretty_assertions` to `1.2.0` - ([efc2392](https://gitlab.com/uillinn/uillinn/commit/efc239201dff266131c82d93b814854a2176cb12)) - Nifou
#### Style
- **(fmt)** run `cargo fmt` - ([90035e4](https://gitlab.com/uillinn/uillinn/commit/90035e4b11cde11a766ceee7e9dc011fb1ebe4a4)) - Nifou
- **(fmt)** run `cargo fmt` - ([b4cdd1f](https://gitlab.com/uillinn/uillinn/commit/b4cdd1f55ead6a40e3e51c78f4dd8e44e57f452a)) - Nifou
#### Tests
- improve test coverage - ([642c3e3](https://gitlab.com/uillinn/uillinn/commit/642c3e30dfe3643cd5cf66c6cbb36822a4ee92e4)) - Nifou
- increase code coverage (+ remove useless code) - ([35f8045](https://gitlab.com/uillinn/uillinn/commit/35f80453e53095a4c9dd5385c3e753d31d3b8366)) - Nifou
- add tests for `Ellipse` panics - ([03e8a86](https://gitlab.com/uillinn/uillinn/commit/03e8a862820944b5d1b72d234962884f2535f548)) - Nifou
- - -

## [0.7.1](https://gitlab.com/uillinn/uillinn/compare/0.7.0..0.7.1) - 2022-03-12
#### Bug Fixes
- fix clippy warnings - ([82b9cfe](https://gitlab.com/uillinn/uillinn/commit/82b9cfeece2b199df8fa73555e50af2c2cda8afd)) - Nifou
#### Documentation
- **(readme)** update the readme - ([3c885c6](https://gitlab.com/uillinn/uillinn/commit/3c885c69ed4a82d3c9b1a331ed1c490be48e9371)) - Nifou
#### Miscellaneous Chores
- update `cargo`'s configuration file - ([28ed364](https://gitlab.com/uillinn/uillinn/commit/28ed36457b3beaf8eef80a93ee25252270b41dee)) - Nifou
- remove `uillinn_parser` - ([d5f8c11](https://gitlab.com/uillinn/uillinn/commit/d5f8c11ea55775ff870a4507680b8f32151d1c01)) - Nifou
- remove `uillinn_render_desktop` - ([8bf7ae6](https://gitlab.com/uillinn/uillinn/commit/8bf7ae6f29be71d8670f5d73436961346e5fc276)) - Nifou
- - -

## [0.7.0](https://gitlab.com/uillinn/uillinn/compare/0.6.0..0.7.0) - 2022-02-16
#### Bug Fixes
- **(uillinn_render_desktop)** use `EventLoop::new` instead of `EventLoop::new_any_thread` - ([9779fc5](https://gitlab.com/uillinn/uillinn/commit/9779fc5bcff67346492fcc44012718eb1edc36c6)) - Nifou
- update Cog's configuration - ([6e2761c](https://gitlab.com/uillinn/uillinn/commit/6e2761c04b3895d90bd8379febe302018bdc0158)) - Nifou
#### Continuous Integration
- **(docs)** fix `pages` pipeline - ([21feae1](https://gitlab.com/uillinn/uillinn/commit/21feae1378b4f140e18d0cf45f36645e7e40583c)) - Nifou
- install `libwebkit2gtk-4.0-dev` - ([5f20b11](https://gitlab.com/uillinn/uillinn/commit/5f20b11af80acc39557122d8d8cd74761ced1bbd)) - Nifou
#### Documentation
- **(readme)** rephrase the introduction and clarify `Getting started` instructions - ([35d8d3b](https://gitlab.com/uillinn/uillinn/commit/35d8d3b6411d003adc1256b8c05f93f40e6b2a40)) - Nifou
#### Features
- **(uillinn_render_desktop)** conveniently export `wry`'s `Result` and `Error` types - ([9ec6b03](https://gitlab.com/uillinn/uillinn/commit/9ec6b032deb2392596c5e35ed1e7c4d1897dc135)) - Nifou
#### Miscellaneous Chores
- **(cog)** add pre/post bump hooks in `cog`'s config - ([01a2442](https://gitlab.com/uillinn/uillinn/commit/01a2442ff69bca733e7cec48ba6bd4eaed29d991)) - Nifou
- **(version)** 0.6.0 - ([2a98c06](https://gitlab.com/uillinn/uillinn/commit/2a98c063e7c9d7c3e485992529055c17033b134a)) - Nifou
- add the `Cargo.lock` file to the `.gitignore` - ([ed665a7](https://gitlab.com/uillinn/uillinn/commit/ed665a76588bfdf4b7600e493e6e9d275ab15b73)) - Nifou
- manually bump versions - ([d1676a3](https://gitlab.com/uillinn/uillinn/commit/d1676a3fd282fd3296e2807aa90530537ea9e445)) - Nifou
#### Refactoring
- **(uillinn_render_web)** collect all additional styles into a unique `<style></style>` container - ([ac2d64e](https://gitlab.com/uillinn/uillinn/commit/ac2d64ee3f2811828c78664d128ac1e6828c4324)) - Nifou
- implement `Display` for `Alignment` - ([5ecc3bf](https://gitlab.com/uillinn/uillinn/commit/5ecc3bfc14bf14b70f2299d79d3b9887c9830fd2)) - Nifou
#### Style
- **(fmt)** run `cargo fmt` - ([54b9918](https://gitlab.com/uillinn/uillinn/commit/54b99186e021e146a377247dcfaf435f10369a84)) - Nifou
#### Tests
- **(uillinn_render_desktop)** completely ignore this render in the coverage report - ([6fb4be1](https://gitlab.com/uillinn/uillinn/commit/6fb4be108b3cae31a7781e392190bf20e07f1c2d)) - Nifou
- - -

## 0.6.0 - 2022-02-15


### Tests

47c392 - completely ignore this render in the coverage report - Nifou

8a721c - remove assertions in the tests of the `Paragraph` component as their result can vary between platforms - Nifou


### Features

b5ea0c - conveniently export `wry`'s `Result` and `Error` types - Nifou

b0ab0f - add an associated type `Result` to the trait `Render` used as the return type of the `Render::render_app` method + REMOVE the `Render::render_app_fs` and `Render::render_to_string` methods - Nifou

92a6dc - display the name of the application as the name of the opened window - Nifou

7d5934 - start creating a render for windowed desktop software - Nifou


### Continuous Integration

8c516b - install `libwebkit2gtk-4.0-dev` - Nifou

ca8cc3 - don't include documentation of dependencies - Nifou

8c11b3 - fix bad regex (mixed with shell escaping) - Nifou


### Style

92931d - run `cargo fmt` - Nifou

a8a0b1 - run `cargo fmt` - Nifou


### Documentation

2e5c8e - add a `Not supported` section in the crate documentation - Nifou

d0d5b9 - update the `Getting started` instructions - Nifou

c6f0dc - update the list of renders - Nifou

5629e2 - update the readme - Nifou


### Miscellaneous Chores

b4dbf7 - add pre/post bump hooks in `cog`'s config - Nifou

a30002 - fix Clippy warnings - Nifou


### Bug Fixes

15bb11 - use `EventLoop::new` instead of `EventLoop::new_any_thread` - Nifou


### Refactoring

7b348f - collect all additional styles into a unique `<style></style>` container - Nifou

fc75ac - implement `Display` for `Alignment` - Nifou

fbc91c - use global default styles defined in the `<head>` element (+ use `span` instead of `p` for rendering single line `Text`s) - Nifou

1df19f - take a `&App` INSTEAD OF a `App` in the `render_app` method of the `Render` trait - Nifou


- - -
## 0.5.1 - 2022-02-04


### Documentation

25b6b5 - add a note stating that the project is experimental - Nifou

4a5631 - add more documentation about `Pos::from_str` - Nifou


### Continuous Integration

3a82f7 - fix escape characters - Nifou

be15e4 - don't count `todo!()` and `unreachable!()` lines as uncovered - Nifou


### Style

94a95e - run `cargo fmt` - Nifou

ab979a - run `cargo fmt` - Nifou


### Tests

4d11be - add some `should_panic` tests - Nifou

874986 - add more tests for colors - Nifou

3814cc - add a test for `line_height`s of `Paragraph`s - Nifou

789121 - improve coverage - Nifou

598614 - improve coverage results by adding a marker in grcov for ignoring lines - Nifou

28271e - add more tests - Nifou

027b32 - add more tests - Nifou

406021 - add more tests - Nifou

5f03cb - add more tests - Nifou


### Refactoring

4b6da1 - fix Clippy warnings - Nifou

7fbe85 - add checks in the `Color::lighten` and `Color::darken` functions + make the `hsl_to_rgb` function more idiomatic - Nifou

daa9be - use better and more precise panic messages in the `pairs_to_components` function - Nifou


### Bug Fixes

b1b908 - trim input when parsing numbers in the `pairs_to_component` function - Nifou

a99d19 - fix useless `m0,0` added to `Path` data when data.len() == 2 - Nifou


- - -
## 0.5.0 - 2022-01-30


### Style

8247b2 - run `cargo fmt` - Nifou

b3ee8e - run `cargo fmt` - Nifou


### Performance Improvements

ee0c86 - use a font cache and prevent `.clone()`ing when generating text paths (8x speed gain) - Nifou


### Features

be3051 - implement `From<&str>` for `Color` - Nifou

3ab8f5 - add a REQUIRED field `bounding_box` to the `Path` structure - Nifou

8dbfb1 - support hexadecimal colors - Nifou

1fcc20 - add a function `Path::from_svg_path` to convert an SVG path to a `Path` structure - Nifou

d9b5f5 - support the `start_angle`, `end_angle` and `ratio` properties of an `Ellipse` in a `.uill` file - Nifou

b2541e - add a `FULL` constant used to let things fill the parent area easily - Nifou

e35411 - if a path data starts with a move command, the useless `m0,0` added before is removed - Nifou

b9f69e - support multiple border radii for rectangles + remove support of multiple border styles, sizes and colors for rectangles - Nifou

09dc68 - use design-tool compatible ellipses - Nifou

0b03a4 - create a dedicated `FontFamilyList` structure - Nifou

1a1804 - support setting multiple font families as fallback - Nifou

f8c48a - support paragraphs - Nifou

08d9bc - add the `line-height` property to `Paragraph`s - Nifou

937e95 - add a render for SVG images - Nifou

8cd2d1 - use `line_height` AND Flexbox to center texts in the `uillinn_render_web` crate (+ add the `self` argument to all methods of the `Render` trait) - Nifou

9e9a28 - add a `close` method to the `Path` component to explicitly close the path and REMOVE the `prevent_close` method - Nifou

057be0 - add a `_to` variant for each `_by` method in `Path`s - Nifou


### Tests

d97cdd - fix tests - Nifou

aea590 - fix tests due to use of `f32`s instead of `usize`s - Nifou


### Miscellaneous Chores

eb8fa9 - update copyright year - Nifou


### Refactoring

bf094c - implement `Eq` for `FontFamilyList` - Nifou

2f904f - run `cargo fmt` and fix Clippy warnings - Nifou

80ec47 - using the `ScreenBased` or `Percent` units for the size of `Ellipse`s is not supported for this render - Nifou

127fe8 - create a `format_id` function to format IDs - Nifou

4b988c - separate per-render defaults and global Uillinn defaults + use the `sans-serif` font for texts by default + rectangles and ellipses are filled by default - Nifou

f8d3c3 - rename the `border_radius` property of `Rectangle`s to `rounded` - Nifou

3b17bf - run `cargo fmt` and fix Clippy warnings - Nifou


### Bug Fixes

c94ea4 - fix Clippy warnings - Nifou

3e1649 - fix border radii with big values using the `corner overlap` rule (defined by the W3C) - Nifou

dde1c4 - remove the `rotate` property for the `Path` component (+ fix bad rotation of the `Rectangle` component in `uillinn_render_svg`) - Nifou

a7ad7d - fix blurry borders on rotated `Path`s - Nifou

9f6c5c - prevent indexing from triggering an `index out of bound` error by adding a condition during the building of a `Path` component - Nifou

04fa8f - use `rgb(0, 0, 0)` (real black) for the `Black` color - Nifou

b4133a - fix the default fill of rectangles on the web - Nifou

145372 - remove the `BorderStyle` enum - Nifou

12a257 - remove `Text.wrap` - Nifou

dadeda - fix Clippy warnings - Nifou

452107 - fix bad ScreenBased behavior - Nifou

201a10 - rename the `Circle` component to `Ellipse` - Nifou

a81d18 - use f32 instead of isize to represent points in `Path`s - Nifou

350e00 - start paths using relative coordinates - Nifou

06c80c - fix Clippy warnings - Nifou


### Documentation

59c725 - update the readme - Nifou

2841a7 - update the code example of the SVG render - Nifou

531961 - update the list of renders - Nifou

d7cf01 - update git references - Nifou


- - -
## 0.4.0 - 2021-12-18


### Tests

72d24e - fix failing tests - Nifou


### Features

ea5436 - add a parser for a custom simple syntax - Nifou

67a609 - implement the `FromStr` trait for several structures - Nifou

556884 - add a name field to each component - Nifou


### Refactoring

8d3c5a - implement `Clone` for all components - Nifou

ecd138 - store `name`, `description` and `lang` using `String` and not `&'static str` in the `App` structure - Nifou

2ea5c1 - implement `PartialEq` for all components and the `App` structure - Nifou


### Style

b38924 - run `cargo fmt` - Nifou


### Documentation

9f4012 - add git branch name in cargo configuration file - Nifou


- - -
## 0.3.0 - 2021-11-24


### Documentation

c85d21 - add a note about layers in the documentation of the `Component::child` method - Nifou

ddcc2a - rename the `move_to`, `line_to`, `x_line`, `y_line`, `cubic_bezier`, `quadratic_bezier` methods of the `Path` component to `move_by`, `line_by`, `x_line_by`, `y_line_by`, `cubic_by`, `quadratic_by` (+ add some documentation) - Nifou


### Features

b8ac5c - add `M0,0` to the start of each `Path` data (+ add some documentation to the `Path` component) - Nifou

709324 - rename `ComponentBehavior` to `CustomComponent` and make it public (+ add an example of how to use it) - Nifou

cc5333 - add an alias `CENTER` to `Pos::CenterPx(0)` - Nifou

f71557 - add a `rotation` property to each component - Nifou

caff2c - add own color manipulation functions (+ drop `css-colors` dependency) - Nifou

58ebd8 - make the `break_word` property optional - Nifou

63d119 - implement `darken` and `lighten` for colors (+ drop support for hexadecimal colors) - Nifou

ae6509 - add a method to prevent implicit closing of a `Path` - Nifou


### Bug Fixes

aea493 - fix Clippy warnings - Nifou

0489ca - fix rgb to hsl conversion when the color is near black - Nifou

812e86 - fix Clippy warnings - Nifou

c16185 - fix white color rgb code - Nifou


- - -
## 0.2.0 - 2021-11-10


### Refactoring

1501ba - implement `Debug` for `Component` and `App` - Nifou


### Features

dc43c2 - break words in paragraphs - Nifou

224687 - control alignment in `Text`s and `Paragraph`s - Nifou


### Style

19066b - run `cargo fmt` - Nifou

e6b81f - run `cargo fmt` - Nifou


### Bug Fixes

3702e7 - directly use integers as pixels in `ScreenBased::new` (using `From` trait) - Nifou


### Documentation

220d11 - add a note about the order of `ScreenBased` rules - Nifou


### Build system

ea78cc - switch crates to Rust 2021 edition - Nifou


- - -
## 0.1.0 - 2021-11-07


### Features

b309e4 - allow specifying screen-dependent styles - Nifou

2f5ff6 - allow computing the hash of a `Component` - Nifou

5bf03d - allow computations on `Unit` - Nifou

edc4f9 - change layout system organization (+ you can now center with some margin) - Nifou

6b20dc - create a `Color` enum for using colors with a type - Nifou

4f028d - add a new component: a `Path` (compatible with SVG paths) - Nifou

7ad841 - add some parameters for setting component borders - Nifou

b5767d - allow specifying border radius independently - Nifou

278f1c - add option for wrapping `Text` components - Nifou

82cd75 - add scrollable areas (+ remove useless container wrapping all elements) - Nifou

bc3423 - add `Left`, `Right`, `Top` and `Bottom` positions and `FullParent` size - Nifou

4ffb18 - add a new component: a `Paragraph` with multiple lines of text (+ hide overflowing content of the `Text` component + remove global styles inserted in the `<head>` of applications) - Nifou

bf473c - add a position for centering easily - Nifou

60fa6f - specify layout using pixels and percents - Nifou

e6b4d2 - add default global styles for text - Nifou

3c17cb - implement keeping ratio in layout system - Nifou

168cd8 - use percents instead of CSS grids - Nifou

5cd0d3 - add pixel sized grids - Nifou

45643b - add a new component: a circle - Nifou

a8e30e - specify `font_size`, `font_weight` and `border_radius` properties using numbers and not strings - Nifou

acb452 - add the property `border_radius` to the rectangle component - Nifou

54d156 - add `font_family`, `font_size` and `font_weight` properties to the text component - Nifou

a6c145 - choose text color (+ separate file in `uillinn_render_web` for just rendering a component) - Nifou

6ac91b - define the size of each row and column in the layout - Nifou

4b1ee0 - implement `Display` for `Layout`, better panicking message when an area is not found - Nifou

439159 - create layouts using names (+ fix CSS grids) - Nifou

8609fa - use x, y, width, height for representing areas - Nifou

977aa0 - add metadata to the `App` structure - Nifou

a31add - add function to `Render` trait for writing the application files to the filesystem - Nifou

00bb06 - introduce a new structure: `App` - Nifou

c3848f - continue working on layouts and introduce a new way of placing items: areas (+ remove area names) - Nifou

257404 - specify layout for each component - Nifou

21b04e - start creating a CSS Grid-based layout system - Nifou

e6e17a - define multiple types of components - Nifou

6a236a - create first renderer: `uillinn_render_web` - Nifou

a6d9df - create `Style`, `StyleList` and `Component` structures - Nifou


### Documentation

ff9b7a - document all items (+ add documentation tests) - Nifou

671494 - fix links of badges - Nifou

d0db0f - better lists, format rust code correctly - Nifou

5efeb2 - add line break after each sentence - Nifou

c0919c - add space between elements of the heading (+ add languages in code blocks) - Nifou

0a788a - cleanup the readme - Nifou

e2b13c - add a readme - Nifou


### Tests

caad7b - add tests for computations on `Unit` - Nifou

ee1e85 - increase test coverage - Nifou

fc303a - add tests for rendering circles - Nifou

68f9d3 - add a test for asserting how a layout is displayed - Nifou


### Miscellaneous Chores

859c2e - add cocogitto configuration file - Nifou

457a69 - fix Gitlab CI configuration file - Nifou

8a3945 - add Gitlab CI configuration file - Nifou

67ce00 - add license, change crate metadata - Nifou


### Refactoring

4787e3 - pass the area to `Render::render_to_string` - Nifou

100cdb - merge `Unit` and `Pos`/`Size` + add `ScreenBased` struct for adding several breakpoints - Nifou

3b00a1 - add helper function for component properties (+ use `style` attribute for paths) - Nifou

8b339b - use `HashMap` instead of `Vec` for representing files - Nifou

b04512 - split layout system into several files (+ create file with utils) - Nifou

263d78 - move base components to their own structure - Nifou

d1c07f - remove styling -> replace with per-component styles - Nifou


### Bug Fixes

e6a398 - prevent HTML tags injection in `Text` and `Paragraph` - Nifou

f22a2b - compute centered position of a component without knowing its size (+ refactor layout styles creation) - Nifou

7def2d - compute hash of components using also the area - Nifou

5cadf7 - fix Clippy warnings - Nifou

5be228 - fix sizes - Nifou

2d85c5 - fix quote escaping - Nifou

dd4d43 - run `cargo fmt` and `cargo clippy` - Nifou


### Continuous Integration

af7588 - run tests during merge requests and generate coverage report on the `main` branch - Nifou

9d6ea5 - run tests on `main` to be able to get the coverage of the whole project - Nifou


### Style

223900 - run `cargo fmt` - Nifou

b2cc1f - remove useless comment - Nifou

277219 - split rendering of components into several files - Nifou


- - -

This changelog was generated by [cocogitto](https://github.com/oknozor/cocogitto).