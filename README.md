<div align="center">
  <a href="https://gitlab.com/uillinn/uillinn">
    <img alt="Uillinn logo" src="https://gitlab.com/uillinn/artwork/-/raw/main/logo/uillinn-logo.svg" width="100" />
  </a>
</div>

<div align="center">Create rich and robust privacy-friendly user-interfaces in Rust</div>

<br />

<div align="center">
  <a href="https://gitlab.com/uillinn/uillinn/blob/main/LICENSE">
    <img alt="License" src="https://img.shields.io/static/v1?label=license&message=MIT&color=green" />
  </a>
  <a href="https://gitlab.com/uillinn/uillinn/-/pipelines">
    <img alt="Pipeline status" src="https://gitlab.com/uillinn/uillinn/badges/main/pipeline.svg" />
  </a>
  <a href="https://gitlab.com/uillinn/uillinn/-/jobs/artifacts/main/download?job=deploy:coverage">
    <img alt="Coverage report" src="https://gitlab.com/uillinn/uillinn/badges/main/coverage.svg" />
  </a>
  <a href="https://deps.rs/repo/gitlab/uillinn/uillinn">
    <img alt="Dependency status" src="https://deps.rs/repo/gitlab/uillinn/uillinn/status.svg" />
  </a>
  <br>
  <a href="https://gitlab.com/uillinn/uillinn">
    <img alt="Number of files" src="https://tokei.rs/b1/gitlab/uillinn/uillinn?category=files" />
  </a>
  <a href="https://gitlab.com/uillinn/uillinn">
    <img alt="Number of lines of code" src="https://tokei.rs/b1/gitlab/uillinn/uillinn?category=code" />
  </a>
  <a href="https://gitlab.com/uillinn/uillinn">
    <img alt="Number of lines of comments" src="https://tokei.rs/b1/gitlab/uillinn/uillinn?category=comments" />
  </a>
  <a href="https://gitlab.com/uillinn/uillinn">
    <img alt="Total number of lines" src="https://tokei.rs/b1/gitlab/uillinn/uillinn" />
  </a>
</div>

<br />

Uillinn is a set of tools including a compiler for creating stable user-interfaces (responsive websites and software). It overrides all the current web-development tools (including CSS and JavaScript) and let you focus on designing the interface instead of struggling to position elements. It generates blazingly fast applications (leveraging native browser optimizations) while minimizing the JavaScript used.

🚧 This project is experimental and should not be used in production.

## Table of contents

- [Introduction](#introduction)
- [Getting started](#getting-started)
- [License](#license)

## Introduction

**As a user**, are you tired to wait for pages to load?
Or you are worried about your privacy online?
Or you don't want that a website send requests without your consent because you have a slow connection?
Or you want to learn how to develop graphical applications but you struggle to learn HTML/CSS/JS?

**As a developer**, are you tired of the thousand ways of positioning elements in CSS?
Or you want to be fully compatible with all browsers?
Or you want to write E2E tests but you find that they aren't much reliable?
Or you have had enough of Single Page Applications (SPAs) and their broken client-side router?

Because installing a software is painful and time-consuming, the majority of programs we use today are web applications.
But the web was not created for that and HTML/CSS websites are being replaced with a unique language: JavaScript.
Your browser is most of the time downloading JavaScript code and that leads to several problems:
- This code **takes time** to be requested from the website, decreasing the performances of it;
- It can **do things** when loaded even if you **have not done anything**;
- It can also **bypass the scope of the page**: for example, it can reload your current tab (`location.reload()`) or spawn a new process on your CPU (`ServiceWorker`s), resulting in bad accessibility.

Unlike software, websites aren't compiled (the source code is not transformed into binary code) but the source code is directly read by web browsers.
This feature could be a limit if we want to write compatible websites because to support a new functionality, every browser needs to change its internal code and all browsers have their own set of functionalities.
It also means that all computations (for displaying a dynamic name for example) are done in your browser.

Now, imagine if you have a compiler **taking a bunch of Rust code, outputting HTML and CSS code and using the browser as a surface to render things**.
You could have very **stable** websites because the compiler could do heavy computations (like how do I center this thing?, how do I make this element responsive?).
They could also be very **efficient** because the compiler knows how to output minimal code.
And JavaScript is no more required!
The compiler can also be used to create a software because you just need a surface to render things.
You can even generate images of websites directly, without using a browser!

Finally, imagine that the Rust (input) code is directly **compatible with the work of designers**, with the same set of properties.
You could easily define design tokens, brand colors and apply them to some shapes.

This tool is Uillinn.

## Getting started

Uillinn uses a modular architecture using [Cargo workspaces](https://doc.rust-lang.org/cargo/reference/workspaces.html).
For now, three crates are released:

- [`uillinn_core`](https://uillinn.gitlab.io/uillinn/uillinn_core) containing the code of abstractions
- [`uillinn_render_web`](https://uillinn.gitlab.io/uillinn/uillinn_render_web) used to transform the abstractions from `uillinn_core` into HTML and CSS
- [`uillinn_render_svg`](https://uillinn.gitlab.io/uillinn/uillinn_render_svg) used to transform the abstractions from `uillinn_core` into an SVG image

### How to use

First, include `uillinn_core` and `uillinn_render_web` in your `Cargo.toml` dependencies:

```toml
[dependencies]
uillinn_core = { git = "https://gitlab.com/uillinn/uillinn.git", branch = "main" }
uillinn_render_web = { git = "https://gitlab.com/uillinn/uillinn.git", branch = "main" }
```

Then, you can start using Uillinn either by generating a file containing the HTML and CSS code:

```rust
use uillinn_core::prelude::*;
use uillinn_render_web::WebRender;
use std::fs;

fn main() {
    // Start by creating a root component with a red text at (0, 0) (by default the text is centered)
    let root = Component::new().child(
        Area::new(0, 0, Percent(100), 50),
        Text::new("Hello world!").color(Red),
    );

    // Then, define an `App` containing some metadata about the application (name, description and language of it)
    let app = App::new("Hello world!", "Just want to say hello", "en", root);

    // Give the application to the `WebRender` and get a string containing the complete page
    let render_result = WebRender.render_app(&app);

    // Finally, write the result to an `index.html` file
    fs::write("index.html", render_result);
}
```

Or by serving the HTML using a Rust server (example with [`vial`](https://vial.rs)):

```rust
use uillinn_core::prelude::*;
use uillinn_render_web::WebRender;
use vial::prelude::*;

routes! {
    GET "/hello/:name" => hello;
}

fn hello(req: Request) -> String {
    let root = Component::new().child(
        Area::new(0, 0, Percent(100), 50),
        Text::new(format!("Hello {}!", req.arg("name").unwrap_or("guest"))).color(Red),
    );

    let app = App::new("Hello world!", "Just want to say hello", "en", root);
    WebRender.render_app(&app)
}

fn main() {
    run!().expect("could not start Vial server");
}
```

## License

Uillinn is released under the [MIT license](https://gitlab.com/uillinn/uillinn/blob/main/LICENSE).
