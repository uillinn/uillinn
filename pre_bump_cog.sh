latest_release=$1
new_release=$2

# Check formatting
cargo fmt -- --check

# Check Clippy warnings
cargo clippy --verbose -- -D warnings

# Bump version in all crates
for crate in crates/*
do
  sed -i -z "s/\[package\]\nname = \"\(.*\)\"\nversion = \"${latest_release}\"/[package]\nname = \"\1\"\nversion = \"${new_release}\"/g" "${crate}/Cargo.toml"
done
